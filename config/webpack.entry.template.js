/* eslint-disable */
import { h, render } from 'preact'

const Module = require('../../src/<%= moduleName %>/<%= moduleName %>').default

const props = {
  key: '<%= key %>',
  variation: '<%= variation %>',
  images: [<%= imagesString %>],
  concept: '<%= concept %>',
  size: '<%= size %>',
  ending: '<%= ending %>'
}

module.exports = render(<Module {...props} />, document.getElementById('root'))

/* eslint-enable */
