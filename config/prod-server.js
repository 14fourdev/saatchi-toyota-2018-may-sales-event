const path = require('path')
const express = require('express')
const compress = require('compression')
const serveIndex = require('serve-index')
const chalk = require('chalk')

// get port
const port = parseInt(process.env.PORT, 10)

// create express instance
const app = express()

// compress files before sending
app.use(compress())

// serve static directory for now
app.use('/', express.static('build'), serveIndex('./build/', { icons: true }))

// handles '/url/path' page refreshes to /index.html - spa
app.get('*', (req, res) => {
  res.sendFile(path.resolve('./build/'))
})

// start server
app.listen(port)

// log to console
console.log(chalk.green(`listening on port ${port}\n`))

// export app for tests
module.exports = app
