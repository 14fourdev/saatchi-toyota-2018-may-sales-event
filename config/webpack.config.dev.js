const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CommonConfig = require('./webpack.config.common.js')
const DevLogs = require('./webpack.plugin.dev-logs.js')
const Banners = require('../src/banner-config').default

// create array of all needed libraries.
const Libraries = require('./utils/banner-libraries').Libraries([]) // eslint-disable-line

// merge dev environment config with common config
const config = Merge(CommonConfig, {
  entry: {
    main: [
      // add webpack-hot-middleware to bundle and
      'babel-polyfill',
      // remove console logs
      'webpack-hot-middleware/client?noInfo=true',
      // add application code to bundle
      path.resolve('./src/_dev/index'),
    ]
  },

  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        exclude: /node_modules/,
        include: path.resolve('./src'),
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'sass-loader',
            options: {
              data: '$size: "dev"; $variation: "dev"; $ending: "dev";',
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: loader => [
                require('autoprefixer')({ browsers: ['last 2 versions'] }), // eslint-disable-line
              ],
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ],
  },

  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  },

  // define webpack output
  output: {
    publicPath: path.resolve('/'),
    filename: '[name].dev.js',
    path: __dirname,
  },

  // fastest for dev
  // - https://webpack.js.org/configuration/devtool/
  devtool: '#eval-source-map',

  plugins: [
    // create global constants
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"'
      }
    }),
    // enable hot reload - webpack-hot-middleware
    new webpack.HotModuleReplacementPlugin(),
    // serve index.html file to client and auto inject script tags
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.ejs',
      inject: true,
      libraries: Libraries,
      minify: { collapseWhitespace: true },
      config: []
    }),
    // This plugin will cause the relative path of the module to be displayed
    new webpack.NamedModulesPlugin(),
    // https://webpack.js.org/plugins/commons-chunk-plugin/
    new webpack.optimize.CommonsChunkPlugin({
      names: ['common'],
      minChunks: Infinity
    }),
    // include custom fun print plugin
    new DevLogs()
  ],
})

module.exports = config
