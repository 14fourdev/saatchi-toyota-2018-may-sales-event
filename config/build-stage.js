require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const webpack = require('webpack')
const getBanners = require('./utils/banners-object')
const chalk = require('chalk')
const DraftLog = require('draftlog').into(console)
const cliSpinners = require('cli-spinners')
const fs = require('fs-extra')
const path = require('path')
const _ = require('lodash')
const notifier = require('node-notifier')

// check for auto deploy requirements. Log details.
if (process.env.STAGE_SITE && process.env.STAGE_SITE.length > 0 && fs.existsSync('/Volumes/WebApps/sites')) {
  console.log(chalk.red(`DELETING (/Volumes/WebApps/sites/${process.env.STAGE_SITE}/public) permanently...\n`))
} else if (!process.env.STAGE_SITE || process.env.STAGE_SITE.length < 1) {
  console.log(chalk.yellow('Define a STAGE_SITE variable in your npm script to auto deploy to lamp.\n'))
} else if (!fs.existsSync('/Volumes/WebApps/sites')) {
  console.log(chalk.yellow('It looks like Lamp is not mounted. I am checking \'/Volumes/WebApps/sites\'. Please mount and try again \n'))
}

// start timer for build
const timeStart = Date.now()
const log = console.draft()
let frame = 0
const { frames } = cliSpinners.dots
function Loading(text) {
  // Next frame
  frame = (frame + 1) % frames.length
  if (text === 'Done') return `${chalk.green(text)} ${chalk.green('\u2714')}  ${chalk.blue(`${Math.round((Date.now() - timeStart) / 1000)}s\n`)}`
  else if (text === 'Error') return `${chalk.red(text)} ${chalk.red('\u2715')}`
  return `${chalk.yellow(text)} ${chalk.blue(frames[frame])}`
}
// start building log
const interval = setInterval(() => log(Loading('Building')), cliSpinners.dots.interval)

// delete build folder and start fresh
fs.removeSync(path.resolve('./stage'))
fs.mkdirp(path.resolve('./stage/public'))
fs.copySync(path.resolve('./config/templates/.htaccess'), path.resolve('./stage/public/.htaccess'))

// get banner object
getBanners()
  .then(banners => new Promise((resolve) => {
    const bannerObjString = JSON.stringify(banners)
    // compile temp entry file
    const compiled = _.template(fs.readFileSync('./config/webpack.entry.stage.js', 'utf8'))
    // write temp entry file with variables
    fs.mkdirpSync('./config/tmp')
    fs.writeFileSync('./config/tmp/webpack.entry.stage.js', compiled({ bannerObjString }))
    // require after above file has been created
    const config = require('./webpack.config.stage') // eslint-disable-line
    resolve(config)
  }))
  .then(config => new Promise((resolve, reject) => {
    // start webpack build process
    webpack(config, (err, stats) => {
      if (err) reject(err)
      resolve()
    })
  }))
  .then(() => new Promise((resolve) => {
    // if auto deploy, auto deploy
    if (process.env.STAGE_SITE && process.env.STAGE_SITE.length > 0 && fs.existsSync('/Volumes/WebApps/sites')) {
      log(Loading('Sending to Lamp'))
      fs.removeSync(`/Volumes/WebApps/sites/${process.env.STAGE_SITE}/public`)
      fs.copySync(path.resolve('./stage/public/'), `/Volumes/WebApps/sites/${process.env.STAGE_SITE}/public`)
      resolve()
    } else {
      resolve()
    }
  }))
  .then(() => {
    // clear log interval and show results
    clearInterval(interval)
    log(Loading('Done'))
    fs.removeSync(path.resolve('./config/tmp'))
    if (process.env.STAGE_SITE && process.env.STAGE_SITE.length > 0 && fs.existsSync('/Volumes/WebApps/sites')) {
      console.log(chalk.blue(`${chalk.green('Available at')} ${process.env.STAGE_SITE}.lamp.14four.com\n`))
      notifier.notify({
        title: 'Done!',
        message: `Stage available at ${process.env.STAGE_SITE}.lamp.14four.com`
      })
    } else {
      notifier.notify({
        title: 'Done!',
        message: 'Stage is done building.'
      })
    }
  })
  .catch(e => console.log(e))
