const fs = require('fs-extra')
const path = require('path')
const glob = require('glob')

// custom plugin to pretty print webpack events
function BuildImages(options) {
  this.options = options
}

function Plugin(compiler) {
  // copy all banner assets
  glob('*', { cwd: path.resolve('src/_assets/'), nodir: true }, (error, files) => {
    files.forEach((file) => {
      fs.copy(
        path.resolve(`./src/_assets/${file}`),
        path.resolve(`./stage/public/assets/img/${file.replace(/\*/g, '-')}`),
        err => (err ? console.log('build-images-stage-plugin: ', err) : null)
      )
    })
  })
  // copy all dev assets
  glob('*', { cwd: path.resolve('src/_dev/assets/img/'), nodir: true }, (error, files) => {
    files.forEach((file) => {
      fs.copy(
        path.resolve(`./src/_dev/assets/img/${file}`),
        path.resolve(`./stage/public/assets/img/${file}`),
        err => (err ? console.log('build-images-stage-plugin: ', err) : null)
      )
    })
  })
}

// create Plugin
BuildImages.prototype.apply = Plugin

module.exports = BuildImages
