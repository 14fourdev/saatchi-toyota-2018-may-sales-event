require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const fs = require('fs-extra')
const _ = require('lodash')
const BuildLogs = require('../webpack.plugin.build-logs.js')
const BuildImages = require('../webpack.plugin.build-images.js')
const BuildConfig = require('../webpack.plugin.build-config.js')
const config = require('../webpack.config.prod.js')

module.exports = Banners => new Promise((resolve) => {
  const bannerConfigs = []
  Object.keys(Banners).forEach((Banner) => {
    const currentBanner = Banners[Banner]
    // compile temp entry file
    const compiled = _.template(fs.readFileSync('./config/webpack.entry.template.js', 'utf8'))
    // correctly named images for output folder and preload function
    console.log(currentBanner.images)
    const namedImages = currentBanner.images.map((img) => {
      const imgArr = img.split('_')
      return `${currentBanner.concept}_${currentBanner.size}_${currentBanner.variation}_${imgArr[3]}`
    })
    // create string of images array for the template files
    const imagesString = `'${namedImages.join("','")}'`
    // write temp entry file with variables
    fs.mkdirpSync('./config/tmp')
    fs.writeFileSync(`./config/tmp/${Banner}.entry.js`, compiled({ imagesString, ...currentBanner }))

    // generate config
    bannerConfigs.push(Merge.smartStrategy({
      'module.entry': 'replace',
      'module.plugins': 'prepend',
      'module.rules': 'prepend'
    })(config, {
      entry: {
        [`${Banner}/${Banner}`]: `${path.resolve(`./config/tmp/${Banner}.entry.js`)}`
      },
      plugins: [
        new HtmlWebpackPlugin({
          inject: false,
          template: 'index.ejs',
          minify: { collapseWhitespace: true },
          filename: `${Banner}/${Banner}.html`,
          scripts: [`${Banner}.js`],
          links: [`${Banner}.css`],
          libraries: currentBanner.libraries,
          config: [currentBanner.config ? 'config.js' : '']
        }),
        new BuildImages({ name: Banner, images: currentBanner.images, namedImages }),
        new BuildConfig({ name: Banner, config: currentBanner.config ? currentBanner.config : '' })
      ],
      module: {
        rules: [
          {
            test: /\.(scss|css)$/,
            exclude: /node_modules/,
            include: [path.resolve('./src'), path.resolve('./config')],
            // extract css bundle to seperate file
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: [
                { loader: 'css-loader' },
                {
                  loader: 'sass-loader',
                  options: {
                    data: `$size: "${currentBanner.size}"; $variation: "${currentBanner.variation}"; $ending: "${currentBanner.ending}";`,
                  }
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: loader => [
                      require('autoprefixer')({ browsers: ['last 2 versions'] }), // eslint-disable-line
                    ],
                  },
                }
              ],
            }),
          },
        ]
      }
    }))
  })
  resolve(bannerConfigs)
})
