require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const Banners = require('../../src/banner-config').default
const getBannerImages = require('./banner-images')
const getBannerLibraries = require('./banner-libraries')

module.exports = () => new Promise((resolve) => {
  const bannerItem = ({ concept, size, config, libraries, className, variation, ending }) => {
    const moduleBase = `${concept}-${size}`
    const obj = {
      key: `${concept}-${size}${variation ? `-${variation}` : ''}${ending ? `-${ending}` : ''}`,
      moduleName: moduleBase,
      images: [],
      concept: concept.toLowerCase(),
      size,
      libraries,
      className,
      addedKWeight: 0
    }

    if (variation) { obj.variation = variation.toLowerCase() } else { obj.variation = '' }
    if (ending) { obj.ending = ending.toLowerCase() } else { obj.ending = '' }
    if (config) { obj.config = config }
    return obj
  }

  const bannersObject = {}
  Object.keys(Banners).forEach((concept) => {
    const currentBanner = Banners[concept]
    const config = Banners[concept].Config
    const libraries = Banners[concept].Libraries
    const className = Banners[concept].Class.name
    currentBanner.Sizes.forEach((size) => {
      if (currentBanner.Variations && currentBanner.Endings) {
        // we want variations and endings
        currentBanner.Variations.forEach((variation) => {
          currentBanner.Endings.forEach((ending) => {
            const sizeObj = bannerItem({ concept, size, config, libraries, className, variation, ending })
            bannersObject[sizeObj.key] = sizeObj
          })
        })
      } else if (currentBanner.Variations) {
        // we want variations
        currentBanner.Variations.forEach((variation) => {
          const sizeObj = bannerItem({ concept, size, config, libraries, className, variation })
          bannersObject[sizeObj.key] = sizeObj
        })
      } else {
        // We only want sizes
        const sizeObj = bannerItem({ concept, size, config, libraries, className })
        bannersObject[sizeObj.key] = sizeObj
      }
    })
  })

  getBannerImages(bannersObject)
    .then(result => getBannerLibraries(result))
    .then(result => resolve(result))
})
