const glob = require('glob')
const path = require('path')

module.exports = Banners => new Promise((resolve) => {
  glob('*', { cwd: path.resolve('src/_assets/'), nodir: true }, (err, files) => {
    Object.keys(Banners).forEach((BannerKey) => {
      files.forEach((originalFile) => {
        const Banner = Banners[BannerKey]
        const file = originalFile.split('_')
        const fileName = process.env.STAGE === 'lamp' ? `/assets/img/${originalFile.replace(/\*/g, '-')}` : originalFile
        const fileConcept = file[0]
        const fileSize = file[1]
        const fileVariation = file[2]
        // variations
        if (fileVariation === Banner.variation && fileSize === Banner.size && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (fileVariation === '*' && fileSize === Banner.size && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (fileVariation === Banner.variation && fileSize === '*' && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (fileVariation === '*' && fileSize === '*' && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (fileVariation === '*' && fileSize === '*' && fileConcept === '*') {
          Banners[BannerKey].images.push(fileName)
        }
        // no variations
        if (file.length === 3 && fileSize === Banner.size && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (file.length === 3 && fileSize === '*' && fileConcept === Banner.concept) {
          Banners[BannerKey].images.push(fileName)
        } else if (file.length === 3 && fileSize === '*' && fileConcept === '*') {
          Banners[BannerKey].images.push(fileName)
        }
      })
      resolve(Banners)
    })
  })
})
