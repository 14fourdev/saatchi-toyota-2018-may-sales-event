module.exports = Banners => new Promise((resolve) => {
  Object.keys(Banners).forEach((BannerKey) => {
    const libraries = []
    let addedKWeight = 0
    Banners[BannerKey].libraries.forEach((library) => {
      if (library === 'gsap' && (Banners[BannerKey].className === 'ToyotaStatic' || Banners[BannerKey].className === 'Static')) {
        libraries.push('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenLite.min.js')
        addedKWeight += 9
        libraries.push('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TimelineLite.min.js')
        addedKWeight += 4
        libraries.push('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/easing/EasePack.min.js')
        addedKWeight += 2
        libraries.push('https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/CSSPlugin.min.js')
        addedKWeight += 15
      }
      if (Banners[BannerKey].className === 'DCM') {
        libraries.push('https://s0.2mdn.net/ads/studio/Enabler.js')
      }
      if (library === 'gsap' && Banners[BannerKey].className === 'DCM') {
        libraries.push('https://s0.2mdn.net/ads/studio/cached_libs/tweenlite_1.20.0_3393be0dce31ad0098cf6549eaa28a6c_min.js')
        addedKWeight += 0
        libraries.push('https://s0.2mdn.net/ads/studio/cached_libs/timelinelite_1.20.0_69767b5d8acb5acac5f8545f23c35618_min.js')
        addedKWeight += 0
        libraries.push('https://s0.2mdn.net/ads/studio/cached_libs/easepack_1.20.0_f9d13d59407792e37168747225359927_min.js')
        addedKWeight += 0
        libraries.push('https://s0.2mdn.net/ads/studio/cached_libs/cssplugin_1.20.0_35574bbb2cf06c392e9036c87b5a8474_min.js')
        addedKWeight += 0
      }
      if (library === 'gsap' && Banners[BannerKey].className === 'Flashtalking') {
        libraries.push('https://cdn.flashtalking.com/frameworks/js/gsap/latest/TweenLite.min.js')
        addedKWeight += 0
        libraries.push('https://cdn.flashtalking.com/frameworks/js/gsap/latest/TimelineLite.min.js')
        addedKWeight += 0
        libraries.push('https://cdn.flashtalking.com/frameworks/js/gsap/latest/easing/EasePack.min.js')
        addedKWeight += 0
        libraries.push('https://cdn.flashtalking.com/frameworks/js/gsap/latest/plugins/CSSPlugin.min.js')
        addedKWeight += 0
      }
    })
    Banners[BannerKey].libraries = libraries // eslint-disable-line
    Banners[BannerKey].addedKWeight = addedKWeight // eslint-disable-line
  })
  resolve(Banners)
})

// used for dev build
module.exports.Libraries = (Banners) => {
  const libraries = [
    'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenLite.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TimelineLite.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/easing/EasePack.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/CSSPlugin.min.js'
  ]
  Banners.forEach((banner) => {
    if (banner === 'DCM') {
      libraries.push('https://s0.2mdn.net/ads/studio/Enabler.js')
    }
  })
  return libraries
}