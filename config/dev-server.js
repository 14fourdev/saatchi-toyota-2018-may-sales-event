require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const webpack = require('webpack')
const path = require('path')
const chokidar = require('chokidar')
const express = require('express')
const proxy = require('http-proxy-middleware')
const glob = require('glob')
const BannersObj = require('./utils/banners-object')
const cache = require('memory-cache')

// get and store banner object
const getBanners = () => {
  BannersObj().then((result) => {
    cache.clear()
    cache.put('bannerObj', result)
  })
}
getBanners()

// get port
const port = parseInt(process.env.PORT, 10)

// define webpack setup as compiler
const compiler = webpack(require('./webpack.config.dev.js'))

// create express instance
const app = express()

// devMiddleware serves the files emitted from webpack over a connect server
const devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: path.resolve('/'),
  quiet: true,
  logLevel: 'silent'
})

// hotMiddleware hot reloads the files served from devMiddleware
const hotMiddleware = require('webpack-hot-middleware')(compiler, {
  log: false,
  heartbeat: 2000,
})

// hot reload when images are added
const watcher = chokidar.watch(path.resolve('./src/_assets/'))
watcher.on('ready', () => {
  watcher.on('all', () => {
    getBanners()
  })
})

// hot reload when the banner config changes
const watcher2 = chokidar.watch(path.resolve('./src/banner-config.js'))
watcher2.on('ready', () => {
  watcher2.on('all', () => {
    getBanners()
  })
})

// handles '/url/path' page refreshes to /index.html - spa
app.use(require('connect-history-api-fallback')())

// serve webpack bundle output
app.use(devMiddleware)

// enable hot-reload and state-preserving
app.use(hotMiddleware)

// send banners
app.get('/api/banners', (req, res) => res.send(cache.get('bannerObj')))

// serve static files
app.use('/assets', express.static('src/_assets'))

// serve preview static files
app.use('/assets', express.static('src/_dev/assets/'))

// proxy image requests
app.use(proxy('**/*.(png|svg|jpg|jpeg|gif|woff|ttf|woff2|eot|otf)', { target: `http://localhost:${port}/assets`, logLevel: 'silent', proxyTimeout: 1000, pathRewrite: { '/.*(?=/)/': '' } })) // , logLevel: 'silent'

// start server
app.listen(port)

// export app
module.exports = app
