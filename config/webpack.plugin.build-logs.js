// const ora = require('ora')
// const cliSpinners = require('cli-spinners')
const chalk = require('chalk')
const DraftLog = require('draftlog').into(console)
const archiver = require('archiver')
const fs = require('fs-extra')
const path = require('path')

// custom plugin to pretty print webpack events
function BuildLogs(options) {
  this.options = options
}
//
// // randomize spinner
// // randomProperty = (obj) => {
// //   const keys = Object.keys(obj)
// //   return obj[keys[keys.length * Math.random() << 0]] // eslint-disable-line
// // }
//
// // create spinner instance
// // const spinner = ora({ spinner: randomProperty(cliSpinners) })
//
// const banners = []
// let count = 0
// let zippedCount = 0
//
// let frame = 0
// const frames = ['-', '\\', '|', '/']
// function Loading(text) {
//   // Next frame
//   frame = (frame + 1) % frames.length
//
//   return chalk.blue(frames[frame]) + ' ' + chalk.yellow(text) + ' ' + chalk.blue(frames[frame]) // eslint-disable-line
// }
//
// const updateLoading = console.draft()
//
// const loadingTest = () => {
//   updateLoading(Loading('Working...'))
// }
//
// // Keeps updating underteminate loading
// const interval = setInterval(loadingTest, 100)
//
// const done = () => {
//   clearInterval(interval)
//   updateLoading(Loading('Done'))
//   console.log('\n')
// }
//
// const printFileSize = (name) => {
//   fs.mkdirp(path.resolve('./build/zipped'))
//   // create a file to stream archive data to.
//   const output = fs.createWriteStream(path.resolve(`./build/zipped/${name}.zip`))
//   const archive = archiver('zip', {
//     zlib: { level: 9 } // Sets the compression level.
//   })
//   output.on('close', () => {
//     console.log(` > ${chalk.blue(name)} -- ${chalk.yellow(`${Math.round((archive.pointer() / 1000), 0)}KB -- External libraries not included`)}`)
//     zippedCount += 1
//     if (zippedCount === banners.length) {
//       done()
//     }
//   })
//   archive.pipe(output)
//   archive.directory(path.resolve(`./build/${name}`), false)
//   archive.finalize()
// }

// create plugin
/* eslint-disable */
BuildLogs.prototype.apply = function (compiler) {
  compiler.plugin('compile', (params) => {
    // count += 1
    // banners.push(this.options.name)
    // first build, compiling
    // if (count === 0) {
    //   // console.log('\n----------------')
    //   // spinner.start('Compiling...')
    //   // console.log(' > ' + chalk.cyan('Compiling...'))
    // }
    //
    // // after first build, adding changes
    // if (count > 0) {
    //   // spinner.start('Updating...')
    //   // console.log('updating')
    // }
  })

  compiler.plugin('compilation', (compilation) => {
    compilation.plugin('optimize', () => {
      // first build, optimizing
      // if (count === 0) {
      //   // spinner.succeed().start('Optimizing...')
      //   // console.log(' > ' + chalk.cyan('Optimizing...'))
      // }
      //
      // // after first build, optimizing changes
      // if (count > 0) {
      //   // spinner.text = 'Optimizing...'
      //   // console.log('optimizing')
      // }
    })
  })

  compiler.plugin('done', (compilation, callback) => {
    // if first build & error, prompt to fix and exit process
    if (compilation.compilation.errors && compilation.compilation.errors.length > 0) {
      // spinner.fail(compilation.compilation.errors[0])
      // console.log('comp error', compilation.compilation.errors)
      // process.exit()
    }

    // if second build & error, print error
    if (compilation.compilation.errors && compilation.compilation.errors.length > 0) {
      // spinner.fail(compilation.compilation.errors[0])
      // console.log('comp error', compilation.compilation.errors)
    }

    // if first build & no error, print app available
    // if (count === 0) {
    //   // spinner.succeed()
    //   // clearInterval(interval)
    //   updateLoading(Loading('Compressing'))
    //   // console.log('\n')
    //   // console.log('Built!')
    //   banners.forEach((name) => {
    //     printFileSize(name)
    //   })
    //   // console.log('\n')
    // }
    // printFileSize(banners[count])

    // if not first build & no error, print successful changes
    // if (count > 0) {
    //   // spinner.succeed('Updated!')
    //   // console.log('Updated')
    // }
    // count++
  })
}
/* eslint-enable */

module.exports = BuildLogs
