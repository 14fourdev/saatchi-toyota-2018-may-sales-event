require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})
const getBanners = require('./utils/banners-object')
const chalk = require('chalk')
const DraftLog = require('draftlog').into(console)
const cliSpinners = require('cli-spinners')
const fs = require('fs-extra')
const path = require('path')
const _ = require('lodash')
const notifier = require('node-notifier')

// start timer for build
const timeStart = Date.now()
const log = console.draft()
let frame = 0
const { frames } = cliSpinners.dots
function Loading(text) {
  // Next frame
  frame = (frame + 1) % frames.length
  if (text === 'Done') return `${chalk.green(text)} ${chalk.green('\u2714')}  ${chalk.blue(`${Math.round((Date.now() - timeStart) / 1000)}s\n`)}`
  else if (text === 'Error') return `${chalk.red(text)} ${chalk.red('\u2715')}`
  return `${chalk.yellow(text)} ${chalk.blue(frames[frame])}`
}
// start building log
const interval = setInterval(() => log(Loading('Reading Config')), cliSpinners.dots.interval)

// get banner object
getBanners()
  .then(banners => new Promise((resolve) => {
    // get all module names
    const modules = []
    Object.keys(banners).forEach((banner) => {
      modules.push(banners[banner].moduleName)
    })
    const allModules = [...new Set(modules)] // this gets all unique values in modules

    // get all directories in src
    const allDirs = []
    const isDirectory = source => fs.lstatSync(source).isDirectory()
    const getDirectories = source =>
      fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory)
    const dirs = getDirectories(path.resolve('./src/'))
    dirs.forEach((dir) => {
      const name = dir.split('/')[dir.split('/').length - 1]
      allDirs.push(name)
    })

    // compare two arrays and return array of uncreated modules
    const newModules = allModules
    allDirs.forEach((name, index) => {
      const match = newModules.indexOf(name)
      if (match > -1) {
        newModules.splice(match, 1)
      }
      if (index === allDirs.length - 1) {
        resolve(newModules)
      }
    })
  }))
  .then(newModules => new Promise((resolve, reject) => {
    log(Loading('Creating New Files'))
    // create new files for each module
    newModules.forEach((module) => {
      const concept = module.split('-')[0]
      const size = module.split('-')[1]
      fs.mkdirpSync(path.resolve(`./src/${module}`))
      const compiledJS = _.template(fs.readFileSync('./config/templates/concept-size-template/concept-size.js', 'utf8'))
      const compiledSCSS = _.template(fs.readFileSync('./config/templates/concept-size-template/concept-size.scss', 'utf8'))
      fs.writeFileSync(path.resolve(`./src/${module}/${module}.js`), compiledJS({ concept, size }))
      fs.writeFileSync(path.resolve(`./src/${module}/${module}.scss`), compiledSCSS({ concept, size }))
    })
    resolve()
  }))
  .then(() => {
    // clear log interval and show results
    clearInterval(interval)
    log(Loading('Done'))
    notifier.notify({
      title: 'Done!',
      message: 'New banners are built.'
    })
  })
