require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const BuildImagesStage = require('./webpack.plugin.build-images-stage.js')
const CommonConfig = require('./webpack.config.common.js')
const Libraries = require('./utils/banner-libraries').Libraries([]) // eslint-disable-line

const config = Merge.smart(CommonConfig, {
  // specify bundle output config
  entry: {
    main: [
      // add webpack-hot-middleware to bundle and
      'babel-polyfill',
      // add application code to bundle
      path.resolve('./config/tmp/webpack.entry.stage.js'),
    ]
  },
  resolve: {
    // automatically resolve these extensions when importing
    // ex : import file from './modules/file.js' => import file from './modules/file'
    extensions: ['.js', '.jsx'],
    // alias styles, modules, and state
    alias: {
      'react': 'preact-compat', // eslint-disable-line
      'react-dom': 'preact-compat',
      richload: path.resolve('./src/_types/richLoad-dev.js')
    }
  },
  output: {
    publicPath: '/',
    filename: '[name].js',
    path: path.resolve('./stage/public')
  },
  module: {
    rules: [
      {
        // es6 -> es5
        test: /\.(jsx?|js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
        include: [path.resolve('./src'), path.resolve('./config')],
      },
      {
        // eslint all jsx and js files
        // before compilation
        test: /\.(jsx?|js)$/,
        exclude: /node_modules/,
        include: path.resolve('./src'),
        use: 'eslint-loader',
        enforce: 'pre',
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: '/assets/fonts/'
          }
        }]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              emitFile: false
            }
          }
        ]
      },
      {
        test: /\.(scss|css)$/,
        exclude: /node_modules/,
        include: path.resolve('./src'),
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'sass-loader',
            options: {
              data: '$size: "dev"; $variation: "dev"; $ending: "dev";',
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: loader => [
                require('autoprefixer')({ browsers: ['last 2 versions'] }), // eslint-disable-line
              ],
            },
          },
        ],
      },
    ]
  },
  plugins: [
    // create global constants
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
        STAGE: '"lamp"'
      }
    }),
    // https://webpack.js.org/plugins/module-concatenation-plugin/
    // new webpack.optimize.ModuleConcatenationPlugin(),
    // plugin to extract css into single file
    new ExtractTextPlugin({ filename: '[name].css', allChunks: true, }),
    // new UglifyJsPlugin({
    //   cache: true,
    //   parallel: true,
    //   uglifyOptions: {
    //     toplevel: true,
    //     ie8: true
    //   },
    //   extractComments: () => null,
    // }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: 'index.ejs',
      scripts: [],
      links: [],
      libraries: Libraries,
      minify: { collapseWhitespace: true },
      config: []
    }),
    new BuildImagesStage()
  ]
})

module.exports = config