const fs = require('fs-extra')
const path = require('path')

// custom plugin to pretty print webpack events
function BuildImages(options) {
  this.options = options
}

function Plugin(compiler) {
  this.options.images.forEach((image, index) => {
    fs.copy(
      path.resolve(`./src/_assets/${image}`),
      path.resolve(`./build/${this.options.name}/${this.options.namedImages[index]}`),
      err => (err ? console.log('build-images-plugin: ', err) : null)
    )
  })
}

// create Plugin
BuildImages.prototype.apply = Plugin

module.exports = BuildImages
