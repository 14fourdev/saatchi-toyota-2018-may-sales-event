require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CommonConfig = require('./webpack.config.common.js')

const config = Merge.smart(CommonConfig, {
  // specify bundle output config
  output: {
    publicPath: '',
    filename: '[name].js',
    path: path.resolve('./build')
  },
  module: {
    rules: [
      {
        // es6 -> es5
        test: /\.(jsx?|js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
        include: [path.resolve('./src'), path.resolve('./config')],
      },
      {
        // eslint all jsx and js files
        // before compilation
        test: /\.(jsx?|js)$/,
        exclude: /node_modules/,
        include: path.resolve('./src'),
        use: 'eslint-loader',
        enforce: 'pre',
      }
    ]
  },
  plugins: [
    // create global constants
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    // https://webpack.js.org/plugins/module-concatenation-plugin/
    new webpack.optimize.ModuleConcatenationPlugin(),
    // plugin to extract css into single file
    new ExtractTextPlugin({ filename: '[name].css', allChunks: true, }),
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      uglifyOptions: {
        toplevel: true,
        ie8: true
      },
      extractComments: () => null,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    })
  ]
})

module.exports = config
