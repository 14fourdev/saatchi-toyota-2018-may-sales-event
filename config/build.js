require('babel-register')({
  presets: ['es2015', 'stage-3'],
  plugins: ['transform-class-properties']
})

const webpack = require('webpack')
const getConfigs = require('./utils/webpack-array')
const getBanners = require('./utils/banners-object')
const chalk = require('chalk')
const DraftLog = require('draftlog').into(console)
const cliSpinners = require('cli-spinners')
const archiver = require('archiver')
const fs = require('fs-extra')
const path = require('path')
const notifier = require('node-notifier')

const timeStart = Date.now()

// delete build folder and start fresh
fs.removeSync(path.resolve('./build/'))

const log = console.draft()

let frame = 0
const { frames } = cliSpinners.dots
function Loading(text) {
  // Next frame
  frame = (frame + 1) % frames.length
  if (text === 'Done') return `${chalk.green(text)} ${chalk.green('\u2714')}  ${chalk.blue(`${Math.round((Date.now() - timeStart) / 1000)}s`)}`
  else if (text === 'Error') return `${chalk.red(text)} ${chalk.red('\u2715')}`
  return `${chalk.yellow(text)} ${chalk.blue(frames[frame])}`
}

const interval = setInterval(() => log(Loading('Building')), cliSpinners.dots.interval)

console.log('') // break

function BannerWorker(queueName, name) {
  this.name = name
  this.TAG = `[${chalk.yellow(queueName)}]`
  this.status = console.draft()
  this.working = false
  this.idle()
}

/* eslint-disable */
BannerWorker.prototype.isWorking = function () { return this.working }

BannerWorker.prototype.idle = function () {
  this.working = false
  this.status(chalk.dim(this.TAG), chalk.dim('idle'))
}

BannerWorker.prototype.updateSpinner = function (text, frame) {
  this.status(this.TAG, chalk.green(text), frame)
}

BannerWorker.prototype.run = function (text) {
  const self = this
  // setTimeout(() => {
  self.working = true
  if (text === 'failed') {
    this.status(chalk.dim(this.TAG), chalk.dim('failed'))
  } else {
    self.status(self.TAG, chalk.green(text))
  }
  // }, 150 + (Math.random() * 400))
}
/* eslint-enable */

// Create multiple queues
const workers = []
let keys = []
let bannersObj = {}

getBanners()
  .then((banners) => {
    bannersObj = banners
    keys = Object.keys(banners)
    const longest = keys.reduce((a, b) => (a.length > b.length ? a : b)).length
    for (let k = 0; k < keys.length; k += 1) {
      workers.push(new BannerWorker(`${banners[keys[k]].key}:`.padEnd(longest + 1), banners[keys[k]].key))
    }
    workers.forEach((worker) => {
      worker.run('Compiling...')
    })
    return banners
  })
  .then(result => getConfigs(result))
  .then(configs => new Promise((resolve, reject) => {
    webpack(configs, (err, stats) => {
      if (err) reject(err)
      const info = stats.toJson()
      if (stats.hasWarnings()) {
        reject(info.warnings[0])
      } else if (stats.hasErrors()) {
        reject(info.errors[0])
      }

      workers.forEach((worker) => {
        worker.idle()
      })
      resolve()
    })
  }))
  .then(() => new Promise((resolve) => {
    workers.forEach((worker, index) => {
      worker.run('Packing...')
      fs.mkdirp(path.resolve('./build/zipped'))
      // create a file to stream archive data to.
      const output = fs.createWriteStream(path.resolve(`./build/zipped/${worker.name}.zip`))
      const archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
      })
      output.on('close', () => {
        let fileSize = Math.round((archive.pointer() / 1000), 0)
        fileSize += bannersObj[worker.name].addedKWeight
        worker.run(`${chalk.blue(`${fileSize}KB`)}`)
        if ((index + 1) === workers.length) {
          resolve()
        }
      })
      archive.pipe(output)
      archive.directory(path.resolve(`./build/${worker.name}`), false)
      archive.finalize()
    })
  }))
  .then(() => {
    clearInterval(interval)
    log(Loading('Done'))
    console.log('')
    fs.removeSync(path.resolve('./config/tmp/'))
    notifier.notify({
      title: 'Done!',
      message: `Banners built at ${path.resolve('./build/')}/`
    })
  })
  .catch((e) => {
    console.log(e)
    clearInterval(interval)
    workers.forEach((worker) => {
      worker.run('failed')
    })
    setTimeout(() => {
      log(Loading('Error'))
      console.log('')
      fs.removeSync(path.resolve('./config/tmp/'))
    }, 500)
  })
