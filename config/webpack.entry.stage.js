/* eslint-disable */
import { h, render } from 'preact'
import Routes from '../../src/_dev/routes'

const bannerObj = <%= bannerObjString %>

module.exports = render(<Routes bannerObj={bannerObj} />, document.getElementById('root'))

/* eslint-enable */
