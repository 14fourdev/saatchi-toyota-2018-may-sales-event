const fs = require('fs-extra')
const path = require('path')

// custom plugin to pretty print webpack events
function BuildConfig(options) {
  this.options = options
}

function Plugin(compiler) {
  if (this.options.config !== '') {
    fs.copy(
      path.resolve(`./src/${this.options.config}`),
      path.resolve(`./build/${this.options.name}/config.js`),
      err => (err ? console.log('build-config-plugin: ', err) : null)
    )
  }
}

// create Plugin
BuildConfig.prototype.apply = Plugin

module.exports = BuildConfig
