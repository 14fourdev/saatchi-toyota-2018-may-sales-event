/* eslint-disable */
import { h, render } from 'preact'

const Module = require('../../src/<%= moduleName %>/<%= moduleName %>').default

export default render(<Module />, document.getElementById('root'))

/* eslint-enable */
