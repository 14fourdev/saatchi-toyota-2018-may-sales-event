import { h, Component, render } from 'preact'
import { <%= concept %> as BannerClass } from '../banner-config'
import './<%= concept %>-<%= size %>.scss'

class <%= concept %><%= size %> extends BannerClass {
  animation = (timeline) => {
    timeline
      // fade in
      .to(this.refs.banner, 0.3, { autoAlpha: 1 })
      // slide background up
      // .to(this.refs.background1, 3.5, { y: -100, ease: Power1.easeInOut }, 1.5)
  }

  animationComplete = () => {
    // animation finished
  }

  render = () => (
    <div data-ref="banner" className="banner <%= concept %>-300x250 hidden">
      {/* <img data-ref="background1" src={this.sizeImage('background1.jpg')} className="background" />
      <img data-ref="mainCar" src={this.variationImage('main-car.png')} className="background hidden" />
      <img data-ref="cloudA" src={this.sizeImage('cloud-a.png')} className="cloud-a" /> */}
    </div>
  )
}

export default <%= concept %><%= size %>
