# Installation
#### Clone from Git
`git clone git@bitbucket.org:14fourdev/bb-8.git`

#### Install
`npm install`


# Usage
#### Dev mode
`npm run dev`

#### Build banner files
`npm run build`

#### Serve built files in static mode
`npm run serve-static`

#### Build for lamp ( see notes below )
`npm run build-stage`


# Main tools
- [Preact][16]
    - This is a smaller version of [React][17] and it should be exactly the same.
- [gsap(optional)][14]


# Configuration Setup
-- [View the file](https://bitbucket.org/14fourdev/bb-8/src/master/src/banner-config.js)

**Parameters**

-   `key` **[String][8]** Name of banners
-   `params` **[Object][9]** params
    -   `Sizes` ***{ Required }*** **[Array][7]&lt;[strings][8]>** array of strings that defines what sizes should be included in the banner.
    -   `Class` ***{ Required }*** **[Array][7]&lt;[string][8]>** imported class from './\_types'. One of:
        - `Static` - Base banner with no additional functionality
        - `DCM` - Double Click Banners. Auto Includes Enabler
        - `Toyota Static` - Base banner with Toyota Incentive API and toyota config output
    -   `Variations` ***{ Optional }*** **[Array][7]&lt;[strings][8]>** array of strings
    -   `Endings` ***{ Optional }*** **[Array][7]&lt;[strings][8]>**  array of strings that is typically used for Toyota Static banner endings. ( Lease, APR, Default, Cash Back )
    -   `Libraries` ***{ Optional }*** **[Array][7]&lt;[strings][8]>**  automatically includes the proper links in the html head section. Added K Weight is included in output. Switches to cdn's that do not count against file size for double click banners.
        -   `gsap` **[30KB][14]** hljs automatically detect language
    -   `Config` ***{ Optional }*** **[string][8]** a valid file path at ./src/. Typically needed for Toyota Static and Flashtalking


**Example**

```javascript
import ToyotaStatic from './_types/toyota-static'
import DCM from './_types/dcm'

const banners = {
  Cloud: {
    Sizes: ['300x250', '160x600', '728x90'],
    Variations: ['Rav4', 'Camry'], // optional
    Endings: ['Default', 'Zip', 'Static', 'Lease', 'Apr', 'Cash'], // optional
    Class: ToyotaStatic,
    Libraries: ['gsap'],
    Config: 'toyota-config.js' // optional
  },
  Test: {
    Sizes: ['300x250'],
    Variations: ['Rav4'],
    Class: DCM,
    Libraries: ['gsap']
  }
}
```

# Asset Naming and Usage
* All assets should be included in './src/\_assets'
* Folders are not supported
* *Naming dictates where an image is included when it is built.*
* Asterisks are wildcard.
* Underscores separate conventions


### Naming Scheme:
`./src/_assets/{concept||*}_{size||*}_{variation||*}[optional]_filename.(png|svg|jpg|jpeg|gif)`


**Example:** `'cloud_300x250_rav4_background.png'` will only be included in the Cloud, 300x250, Rav4 Banner.

**Example:** `'cloud_160x600_*_background.png'` will be included in all of the cloud 160x600 banners.

**Example:** `'cloud_*_*_background.png'` will be included for all cloud banners.

### Usage:
Every banner has base functions to make using images easier.

**Example:** if you need a variation specific image.

`<img ... src={this.variationImage('background.png')} ... />`    

- the variation, size, and concept will automatically be populated for the banner you are editing.
- ***{ returns }*** `'cloud_300x250_rav4_background.png'` or `'cloud_160x600_camry_background.png'`


**Example:** if you need a size specific image and the variation does not matter.

`<img ... src={this.sizeImage('background.png')} ... />`

- the size and concept will be automatically populated for the banner you are editing.
- ***{ returns }*** `'cloud_300x250_*_background.png'` or `'cloud_160x600_*_background.png'`


**Example:** if you need a concept specific image.

`<img ... src={this.conceptImage('background.png')} ... />`

- the concept will be automatically populated for the banner you are editing.
- ***{ returns }*** `'cloud_*_*_background.png'`


# Parsed scss

- `@if` works for the production build
- `._160x600` works for the development build


```
@if (size('160x600')) {
    ._160x600 {
      background: blue;
    }
}
```
```
@if (variation('rav4')) {
    ._rav4 {
      background: blue;
    }
}
```
```
@if (ending('cloud')) {
    ._cloud {
      background: blue;
    }
}
```

# Class functionality

All classes extend the `./src/_types/base.js` class.

When the banner is mounted to the page, it performes a few actions for you automatically. They include:

- image preloading
    - all images with the correct naming scheme will be automatically preloaded. If an image is requested that does not exist, it will fail after a 1 second timeout and console log the error.

- data-ref string binding
    - this allows you to set a reference to a DOM element such as: `<img data-ref="background" />` and access it in your javascript as `this.refs.background`

- image function setup
    - this allows you use the functions described above. `this.variationImage('background.png')`, `this.sizeImage('background.png')`, `this.conceptImage('background.png')`

- **animation function start**
    - after images have preloaded and data-refs have been set, the function `this.animation` is automatically called. If you have included `gsap` as a library, a `new TimelineLite` will be passed through as a parameter, and the timeline will automatically be played when the images finish preloading. If `gsap` is included, it will also fire the function `this.animationComplete` after the animation has finished.



# Stage Build

**Example:** "build-stage": "cross-env BABEL_ENV=production STAGE=lamp STAGE_SITE=toyota-2018-may-v3 node config/build-stage.js"

**Output:** Available at toyota-2018-may-v3.lamp.14four.com

The `STAGE_SITE` variable defines where your build will be deployed on lamp.
You must have WebApps mounted for this to work. 

# Links

- [gsap cheatsheet][15]

[7]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array

[8]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[9]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[10]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean

[11]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise

[12]: https://developer.mozilla.org/docs/Web/API/Comment/Comment

[13]: http://daringfireball.net/projects/markdown/

[14]: https://greensock.com/gsap

[15]: https://ihatetomatoes.net/wp-content/uploads/2016/07/GreenSock-Cheatsheet-4.pdf

[16]: https://preactjs.com/

[17]: https://reactjs.org/