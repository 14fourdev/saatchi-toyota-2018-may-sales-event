import { h, Component } from 'preact'
import Select from 'react-select'
import './styles/_selector.scss'
import InputGroup from './input-group'
import './styles/plugins/_react-select.scss'

export default class Selector extends Component {
  selectChanged = (event) => {
    const { selectChanged } = this.props
    if (typeof selectChanged === 'function') {
      selectChanged(event)
    }
  }

  render = () => {
    let firstOption = null
    if (this.props.options.length > 0) {
      [firstOption] = this.props.options
    }
    const val = this.props.value || firstOption

    return (
      <div className="selector">
        <InputGroup title={this.props.title} titlePriority={this.props.titlePriority} >
          <Select
            value={val}
            onChange={this.selectChanged}
            options={this.props.options}
            clearable={false}
            resetValue={firstOption}
          />
        </InputGroup>
      </div>
    )
  }
}