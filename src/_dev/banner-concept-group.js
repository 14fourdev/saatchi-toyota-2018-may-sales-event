import { h, Component } from 'preact'
import './styles/_banner-concept-group.scss'

export default class BannerConceptGroup extends Component {
  render = () => (
    <div className={`banner-concept-group concept-${this.props.concept}`}>
      {this.props.banners}
      {this.props.children}
    </div>
  )
}
