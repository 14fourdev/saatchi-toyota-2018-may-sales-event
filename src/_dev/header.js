import { h, Component } from 'preact'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './styles/_header.scss'

export default class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      homeLinkWidth: '200px'
    }
  }

  componentDidMount() {
    this.setLeftImageWidth()
  }

  setLeftImageWidth() {
    const titleWidth = document.querySelector('.header .header-home').clientWidth
    this.setState({ homeLinkWidth: `${titleWidth}px` })   // eslint-disable-line
  }

  render = () => {
    const homeURL = this.props.homeURL || '/'
    const company = this.props.company || ''

    const campaignURL = this.props.campaignURL || '/'
    const campaignTitle = this.props.campaignTitle || 'Campaign'
    const campaignBg = this.props.campaignImg ? `url('${this.props.campaignImg}')` : 'url("/assets/img/campaign_img_default.svg")' // eslint-disable-line

    return (
      <div className="header">
        <div className="header-content">
          <Link to={homeURL} className="header-home">
            <div className="header-home-arrow" />
            <h2 className="header-home-title" >{company}</h2>
          </Link>
          <Link to={campaignURL} className="header-campaign">
            <h3 className="header-campaign-title">{campaignTitle}</h3>
            <div className="header-campaign-img" style={{ backgroundImage: campaignBg }} />
          </Link>
        </div>
        <div className="img-container">
          <div className="left-img" style={{ width: this.state.homeLinkWidth }} />
          <img className="middle-img" src="/assets/img/triangles.svg" />
          <div className="right-img" />
        </div>
      </div>
    )
  }
}

Header.propTypes = {
  homeURL: PropTypes.string,
  company: PropTypes.string,
  campaignURL: PropTypes.string,
  campaignTitle: PropTypes.string,
  campaignImg: PropTypes.string
}
