import { h, Component } from 'preact'
import axios from 'axios'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Preview from './preview'
import BannerPreview from './banner-preview'
import Banners from '../banner-config'

class Routes extends Component {
  constructor(props) {
    super(props)
    const routeInfo = this.makeRoutes()
    this.state = {
      ...routeInfo
    }
  }

  makeRoutes = () => {
    const banners = this.props.bannerObj
    const bannerRoutes = []
    const els = []
    Object.keys(banners).forEach((banner) => {
      const currentBanner = banners[banner]
      const module = require(`../${currentBanner.moduleName}/${currentBanner.moduleName}`).default // eslint-disable-line
      const el = h(module, { ...currentBanner })
      bannerRoutes.push(<Route key={currentBanner.key} path={`/banner/${currentBanner.key}`} component={() => el} />)
      els.push(el)
    })
    return { bannerRoutes, els }
  }

  render = () => (
    <BrowserRouter>
      <div id="app-container">
        <Switch>
          { this.state.bannerRoutes }
          <Route path="/:concept?/:size?/:variation?/:ending?" component={() => <BannerPreview banners={this.state.els} />} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

export default Routes
