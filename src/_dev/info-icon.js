import { h, Component } from 'preact'
import PropTypes from 'prop-types'
import './styles/_info-icon.scss'

export default class InfoIcon extends Component {
  onClick = () => {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick()
    }
  }

  render = () => {
    const clickable = (typeof this.props.onClick === 'function')
    const hasText = !(this.props.text === '' || this.props.text == null)
    return (
      <div
        onClick={this.onClick}
        className="info-icon"
        style={{
          cursor: (clickable ? 'pointer' : ''),
          marginRight: (hasText ? '0.3em' : '0')
        }}
      >
        <img className="info-icon-img small-icon" src={this.props.icon} alt="" />
        <div className="text-container">
          <span>{this.props.text}</span>
        </div>
      </div>
    )
  }
}

InfoIcon.propTypes = {
  text: PropTypes.string
}
