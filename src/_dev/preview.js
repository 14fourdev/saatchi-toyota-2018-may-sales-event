import { h, Component } from 'preact'
import './styles/_preview.scss'

export default class Preview extends Component {
  render = () => (
    <div className="preview">
      { this.props.els }
    </div>
  )
}
