import { h, render } from 'preact'
import ReactDOM from 'react-dom' // eslint-disable-line
import axios from 'axios'
import Routes from './routes'

let bannerObj = {}

const renderIt = Component => ReactDOM.render( // eslint-disable-line react/no-render-return-value
  <Component bannerObj={bannerObj} />,
  document.getElementById('root')
)

axios.get('/api/banners')
  .then((response) => {
    bannerObj = response.data
    // first render
    renderIt(Routes)
  })
  .catch((e) => {
    console.log('failed to get api: ', e)
  })

// on hot changes, run render function
if (module.hot) module.hot.accept('./routes', () => renderIt(Routes))

export default renderIt
