import { h, Component } from 'preact'
import './styles/_banner-size-group.scss'

export default class BannerSizeGroup extends Component {
  render = () => (
    <div className={`banner-size-group banner-size-${this.props.size}`}>
      {this.props.banners}
      {this.props.children}
    </div>
  )
}
