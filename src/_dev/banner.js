import Preact, { h, Component, render } from 'preact'
import Util from './scripts/util'
import InfoIcon from './info-icon'
import './styles/_banner.scss'

export default class Banner extends Component {
  constructor(props) {
    super(props)
    this.reloadBanner = this.reloadBanner.bind(this)
    this.state = {
      banner: this.createBannerCopy(),
      width: this.props.banner.attributes.size.split('x')[0],
      height: this.props.banner.attributes.size.split('x')[1],
      duration: 0
    }
  }

  componentDidMount() {
    if (this.banner && this.banner.masterTimeline) {
      this.setState({ duration: this.banner.masterTimeline.totalDuration() })  // eslint-disable-line
    }
  }

  createBannerCopy() {
    return `${this.props.banner.key.toLowerCase()}?rand=${Math.random()}`
  }

  reloadBanner() {
    return new Promise((resolve) => {
      this.setState({
        banner: this.createBannerCopy()
      }, resolve)
    })
  }

  render() {
    let { size } = this.props
    size = size.split('x').join(' X ')

    let duration = null
    if (this.state.duration) {
      duration = <InfoIcon icon="/assets/img/clock.svg" text={`${(this.state.duration).toFixed(2)}s`} />
    }

    return (
      <div className="banner-component">
        <div className="banner-wrapper">
          <iframe src={`/banner/${this.state.banner}`} width={this.state.width} height={this.state.height} frameBorder="0" />
        </div>
        <div className="banner-info">
          <div className="extra">
            <h4 className="subtitle">{size}</h4>
          </div>
          <div className="extra">
            <InfoIcon icon="/assets/img/reload.svg" onClick={this.reloadBanner} />
            {duration}
          </div>
        </div>
      </div>
    )
  }
}
