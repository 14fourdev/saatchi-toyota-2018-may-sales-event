class Util {
  getBannersMatchingConfig = ({ banners, concept, size, variation, ending }) => {
    const bannerList = []
    // Filter the banners based on the concept, size, variation, and ending.
    banners.forEach((banner) => {
      const { attributes } = banner
      // Parallel arrays so we can compare values easier
      const passedValues = [concept, size, variation, ending]
      const banValues = [attributes.concept, attributes.size, attributes.variation, attributes.ending]
      let shouldAdd = true
      for (let i = 0; i < passedValues.length; i += 1) {
        const passed = passedValues[i]
        const ban = banValues[i]
        // Don't add the value if one of the values doesn't match
        if (passed != null && passed !== 'all' && passed !== ban) {
          shouldAdd = false
          break
        }
      }
      // If all the values matched, then we've got a banner we want
      if (shouldAdd) { bannerList.push(banner) }
    })
    return bannerList
  }

  getUniqueAttrListValuePair = ({ key, addGeneric, banners, genericTitle }) => this.getTitleValuePairList({ array: this.getUniqueAttrList({ attr: key, banners }), addGeneric, genericTitle })

  getUniqueAttrObjectList = ({ key, valueKey, addGeneric, banners, genericTitle }) => {
    const array = this.getUniqueAttrList({ attr: key, additionalAttrs: [valueKey], banners })
    let valueList = []
    if (addGeneric && array.length > 0) {
      valueList.push({
        label: genericTitle || 'All',
        value: (genericTitle || 'all')
      })
    }
    valueList = [...valueList, ...array.map(item => ({ label: item[key], value: item[valueKey] }))]
    return valueList
  }

  getUniqueAttrList = ({ attr, additionalAttrs, banners }) => {
    const array = banners.map((el) => { // eslint-disable-line
      let returnVal = additionalAttrs ? {} : ''
      // We're gonna return an object
      if (additionalAttrs) {
        // This is the key we've filtered on
        returnVal[attr] = el.attributes[attr]
        // Add all the other attributes we want
        additionalAttrs.forEach((extraAttr) => {
          returnVal[extraAttr] = el.attributes[extraAttr]
        })
      } else {
        // Just return a string
        returnVal = el.attributes[attr]
      }
      if (returnVal) { return returnVal }
    })

    let uniqueArray = Array.from(new Set(array))
    if (additionalAttrs) {
      uniqueArray = uniqueArray.filter((item, index, self) => index === self.findIndex(t => t[attr] === item[attr]))
    } else {
      // Unique strings
      uniqueArray = uniqueArray.filter(el => el != null)
    }
    return uniqueArray
  }

  capitalize = ({ string }) => string.charAt(0).toUpperCase() + string.slice(1)
  splitPascalCase = ({ string }) => string.replace(/([A-Z][a-z])/g, ' $1').replace(/(\d)/g, ' $1')

  getTitleValuePairList = ({ array, addGeneric, genericTitle, useIndexAsValue = false }) => {
    let newArray = []
    let idIndex = 0
    if (addGeneric && array.length > 0) {
      newArray.push({
        label: genericTitle || 'All',
        value: (useIndexAsValue ? idIndex++ : (genericTitle || 'all'))  // eslint-disable-line
      })
    }
    const mappedArray = array.map((item) => { // eslint-disable-line
      if (item) {
        const label = this.capitalize({ string: item })
        return ({ label, value: useIndexAsValue ? idIndex++ : item })  // eslint-disable-line
      }
    })
    newArray = [...newArray, ...mappedArray]
    return newArray
  }

  guid = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1)
    }
    return `${s4()}-${s4()}-${s4()}-${s4()}-${s4()}-${s4()}`
  }
}

export default new Util()
