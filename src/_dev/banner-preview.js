import { h, Component } from 'preact'
import { Config } from '../banner-config'
import SelectBanner from './select-banner'
import Header from './header'
import './styles/index.scss'

export default class BannerPreview extends Component {
  render = () => (
    <div className="banner-preview">
      <Header
        homeURL="/"
        company={Config.company}
        campaignURL="/"
        campaignTitle={Config.campaignTitle}
        campaignImg=""
      />
      <SelectBanner banners={this.props.banners} />
    </div>
  )
}
