import { h, Component } from 'preact'
import { withRouter } from 'react-router-dom'
import Selector from './selector'
import InputGroup from './input-group'
import BannerSizeGroup from './banner-size-group'
import BannerConceptGroup from './banner-concept-group'
import Banner from './banner'
import Sidebar from './side-bar'
import Util from './scripts/util'
import './styles/_select-banner.scss'

class SelectBanner extends Component {
  constructor(props) {
    super(props)
    const params = this.currentURLParams
    this.state = {
      concept: params.concept,
      size: params.size,
      variation: params.variation,
      ending: params.ending,
      displayLimit: 10
    }
  }

  get conceptList() {
    // Concept list shouldn't change after
    // the first access so we cache it
    if (!this._conceptList) {
      this._conceptList = Util.getUniqueAttrListValuePair({ key: 'concept', addGeneric: true, banners: this.props.banners })
    }
    return this._conceptList
  }

  get sizeList() {
    if (!this._sizeList || this._refreshSize) {
      this._sizeList = Util.getUniqueAttrListValuePair({ key: 'size', addGeneric: true, banners: this.getConfigFilteredBanners({ size: false }) })
      this._refreshSize = false
    }
    return this._sizeList
  }

  get variationList() {
    if (!this._variationList || this._refreshVariation) {
      this._variationList = Util.getUniqueAttrListValuePair({ key: 'variation', addGeneric: true, banners: this.getConfigFilteredBanners({ size: false, variation: false }) })
      this._refreshVariation = false
    }
    return this._variationList
  }

  get endingList() {
    if (!this._endingList || this._refreshEnding) {
      this._endingList = Util.getUniqueAttrListValuePair({ key: 'ending', addGeneric: true, banners: this.getConfigFilteredBanners({ size: false, variation: false, ending: false }) })
      this._refreshEnding = false
    }
    return this._endingList
  }

  get currentURLParams() {
    const routerParams = this.props.match.params
    const paramObject = {
      concept: routerParams.concept || 'all',
      size: routerParams.size || 'all',
      variation: routerParams.variation || 'all',
      ending: routerParams.ending || 'all'
    }

    const hash = window.location.hash.replace('#', '')
    const params = hash.split('?')
    params.forEach((param, index) => {
      const pair = param.split('=')
      if (pair.length === 2) {
        paramObject[pair[0]] = pair[1] // eslint-disable-line
      }
    })
    return paramObject
  }

  get bannersToRender() {
    let banners = []
    if (window.location.pathname !== '/') {
      banners = this.getFilteredAndGroupedBanners({ groupSizes: true, limit: this.state.displayLimit })
    } else {
      banners = (
        <div className="filter-message">
          <h3>Please select an option</h3>
        </div>
      )
    }
    return banners
  }

  get selectMenus() {
    // SELECTORS
    const selectors = []
    if (this.conceptList.length > 0) {
      selectors.push(<Selector value={this.state.concept} title="Concept" key="concepts" options={this.conceptList} selectChanged={this.conceptChanged} />)
    }
    if (this.sizeList.length > 0) {
      selectors.push(<Selector value={this.state.size} title="Size" key="sizes" options={this.sizeList} selectChanged={this.sizeChanged} />)
    }
    if (this.variationList.length > 0) {
      selectors.push(<Selector value={this.state.variation} title="Variation" key="variations" options={this.variationList} selectChanged={this.variationChanged} />)
    }
    if (this.endingList.length > 0) {
      selectors.push(<Selector value={this.state.ending} title="Ending" key="endings" options={this.endingList} selectChanged={this.endingChanged} />)
    }
    // INPUT
    const limitInput = (
      <InputGroup title="Limit">
        <input type="number" value={this.state.displayLimit} onChange={event => this.setState({ displayLimit: event.target.value })} />
      </InputGroup>
    )
    // selectors.push(limitInput)
    return selectors
  }

  get loadMoreButton() {
    if (this.bannersToRender.length > 0 && this.state.displayLimit < this.getConfigFilteredBanners().length) {
      return (<button className="load-more" onClick={this.loadMoreBanners}>Load More</button>)
    }
    return null
  }

  getConfigFilteredBanners = ({ concept = true, size = true, variation = true, ending = true, limit } = {}) => {
    const banners = Util.getBannersMatchingConfig({
      banners: this.props.banners,
      concept: concept ? this.state.concept : null,
      size: size ? this.state.size : null,
      variation: variation ? this.state.variation : null,
      ending: ending ? this.state.ending : null
    })
    if (limit && limit < banners.length) {
      return banners.slice(0, limit)
    }
    return banners
  }

  getConceptGroup = ({ banners, concept }) => {
    const conceptBanners = Util.getBannersMatchingConfig({ concept, banners })
    return <BannerConceptGroup key={concept} concept={concept} banners={conceptBanners} />
  }

  getSizeGroup = ({ banners, size }) => {
    const sizeBanners = Util.getBannersMatchingConfig({ size, banners })
    return <BannerSizeGroup key={size} size={size} banners={sizeBanners} />
  }

  getFilteredAndGroupedBanners = ({ groupSizes, limit } = {}) => {
    const banners = this.getWrappedBanners({ banners: this.getConfigFilteredBanners({ limit }) })
    const concepts = Util.getUniqueAttrList({ attr: 'concept', banners })
    const conceptGroups = []

    concepts.forEach((concept) => {
      const sizeGroups = []
      const conceptBanners = Util.getBannersMatchingConfig({ concept, banners })
      if (groupSizes) {
        const sizeList = Util.getUniqueAttrList({ attr: 'size', banners: conceptBanners })
        sizeList.forEach((size) => { sizeGroups.push(this.getSizeGroup({ banners: conceptBanners, size })) })
      }

      if (sizeGroups.length > 0) {
        conceptGroups.push(<BannerConceptGroup banners={sizeGroups} concept={concept} />)
      } else {
        conceptGroups.push(<BannerConceptGroup banners={conceptBanners} concept={concept} />)
      }
    })
    return conceptGroups
  }

  getWrappedBanners({ banners }) {
    const wrappedBanners = []
    banners.forEach((banner) => {
      const { concept, size, variation, ending } = banner.attributes
      const wrappedBanner = (
        <Banner
          key={banner.key}
          banner={banner}
          concept={concept}
          size={size}
          variation={variation}
          ending={ending}
        />
      )
      wrappedBanners.push(wrappedBanner)
    })
    return wrappedBanners
  }

  loadMoreBanners = () => {
    const displayLimit = parseInt(this.state.displayLimit, 10) + 10
    this.setState({ displayLimit })
  }

  // prevent filtering on banners if current concept doesn't have attributes
  resetFiltersForConcept = () => {
    const banners = Util.getBannersMatchingConfig({
      banners: this.props.banners,
      concept: this.state.concept
    })

    const sizeList = Util.getUniqueAttrListValuePair({ key: 'size', addGeneric: true, banners })
    const variationList = Util.getUniqueAttrListValuePair({ key: 'variation', addGeneric: true, banners })
    const endingList = Util.getUniqueAttrListValuePair({ key: 'ending', addGeneric: true, banners })

    const stateObj = {}
    if (sizeList.length === 0) { stateObj.size = 'all' }
    if (variationList.length === 0) { stateObj.variation = 'all' }
    if (endingList.length === 0) { stateObj.ending = 'all' }
    this.setState(stateObj)
  }

  // URL HANDLING
  changeURL = ({ concept = this.state.concept, size = this.state.size, variation = this.state.variation, ending = this.state.ending } = {}) => {
    let url = `${concept}/${size}/${variation}/${ending}`
    const splitUrl = url.split('/')

    if (splitUrl[0] === 'all' && splitUrl[1] === 'all' && splitUrl[2] === 'all' && splitUrl[3] === 'all') {
      url = ''
    } else if (splitUrl[1] === 'all' && splitUrl[2] === 'all' && splitUrl[3] === 'all') {
      url = `${concept}`
    } else if (splitUrl[2] === 'all' && splitUrl[3] === 'all') {
      url = `${concept}/${size}`
    } else if (splitUrl[3] === 'all') {
      url = `${concept}/${size}/${variation}`
    }

    if (window.location.pathname !== url) {
      this.setState({ concept, size, variation, ending }, () => {
        this.props.history.push(`/${url}`)
      })
    }
  }

  // MENU CHANGE EVENTS
  conceptChanged = (event) => {
    const concept = event.value
    this._refreshSize = true
    this._refreshVariation = true
    this._refreshEnding = true
    this.changeURL({ concept })
    this.resetFiltersForConcept()
  }

  sizeChanged = (event) => {
    this._refreshSize = true
    this._refreshVariation = true
    this._refreshEnding = true
    const size = event.value
    this.changeURL({ size })
  }

  variationChanged = (event) => {
    this._refreshVariation = true
    this._refreshEnding = true
    const variation = event.value
    this.changeURL({ variation })
  }

  endingChanged = (event) => {
    this._refreshEnding = true
    const ending = event.value
    this.changeURL({ ending })
  }

  render = () => (
    <div className="select-banner">
      <div className="select-banner-content">
        <Sidebar>{this.selectMenus}</Sidebar>
        <div className="banner-container">
          {this.bannersToRender}
          {this.loadMoreButton}
        </div>
      </div>
    </div>
  )
}

export default withRouter(SelectBanner)
