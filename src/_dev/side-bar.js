import { h, Component } from 'preact'
import './styles/_side-bar.scss'

export default class Sidebar extends Component {
  render = () => { // eslint-disable-line
    return (
      <div className="sidebar">
        <div className="sidebar-content">
          {this.props.children}
        </div>
      </div>
    )
  }
}
