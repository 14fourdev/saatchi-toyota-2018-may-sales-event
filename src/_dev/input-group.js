import { h, Component } from 'preact'

export default class InputGroup extends Component {
  render = () => {
    const title = this.props.title ? <h3 className="title menu-title">{this.props.title}</h3> : null

    return (
      <div className="input-group">
        {title}
        {this.props.children}
      </div>
    )
  }
}
