import path from 'path'
import ToyotaStatic from './_types/toyota-static'
import Static from './_types/static'
// import DCM from './_types/dcm'

const banners = {
  Deal: {
    Name: 'Deal',
    Sizes: ['300x250', '120x600', '160x600', '300x600', '320x480', '728x90', '768x1024', '970x90'],
    Variations: ['4Runner', '86', 'Avalon', 'Avalon-Hybrid', 'Camry', 'Camry-Hybrid', 'Corolla', 'Corolla-iM', 'Highlander', 'Highlander-Hybrid', 'Landcruiser', 'Prius', 'Prius-Prime', 'Prius-C', 'Prius-V', 'Rav4', 'Rav4-Hybrid', 'Sequoia', 'Sienna', 'Tacoma', 'Tundra', 'Yaris', 'Yaris-iA'],
    Class: ToyotaStatic,
    Libraries: ['gsap'],
    Config: 'toyota-config.js'
  },
  MobileAwareness: {
    Name: 'Awareness - Mobile',
    Sizes: ['300x250', '120x600', '160x600', '300x600', '320x480', '728x90', '768x1024', '970x90'],
    Class: Static,
    Libraries: ['gsap'],
    Config: 'toyota-config.js'
  },
  DesktopAwareness: {
    Sizes: ['300x250', '120x600', '160x600', '300x600', '970x250', '728x90', '970x90'],
    Class: Static,
    Libraries: ['gsap'],
    Config: 'toyota-config.js'
  }
}

const config = {
  company: 'Toyota',
  campaignTitle: 'May Sales Event'
}

export default banners
export const Deal = banners.Deal.Class
export const MobileAwareness = banners.MobileAwareness.Class
export const DesktopAwareness = banners.DesktopAwareness.Class
export const Config = config
