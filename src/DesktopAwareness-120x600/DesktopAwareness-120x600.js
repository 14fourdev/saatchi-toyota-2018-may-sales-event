import { h, Component, render } from 'preact'
import { DesktopAwareness as BannerClass } from '../banner-config'
import VariationAnimations from './VariationAnimations'
import Car from '../Car/CanvasCar'
import LegalOverlay from '../LegalOverlay/LegalOverlay'
import './DesktopAwareness-120x600.scss'

class DesktopAwareness120x600 extends BannerClass {
  componentWillMount() {
    this.cars = {}
    this.delayShowLegal = this.delayShowLegal.bind(this)
    this.interruptShowLegal = this.interruptShowLegal.bind(this)
    this.showLegal = this.showLegal.bind(this)
  }

  animation(timeline) {
    timeline
      .to(this.refs.banner, 0.3, { autoAlpha: 1 })
      .to(this.refs.wipeContainer1, 0.65, { height: '100%', ease: Power1.easeIn })
      .to(this.refs.wipeContainer2, 0.75, { height: '100%', ease: Power1.easeIn }, 0.6)
      .from(this.refs.eventDate, 0.7, { y: -250, ease: Power1.easeOut }, 1.25)
      .from(this.refs.eventCopy, 0.7, { y: -300, ease: Power1.easeOut }, 1.3)
      .add(this.cars.camry.getTimeline(), 1.5)
  }

  animationComplete() {
    // animation finished
  }

  delayShowLegal() {
    this.timer = setTimeout(this.showLegal, 750)
  }

  interruptShowLegal() {
    clearTimeout(this.timer)
  }

  showLegal() {
    this.overlayRef.show()
  }

  render() {
    return (
      <div className="banner DesktopAwareness-120x600 hidden" data-ref="banner">
        <div className="border border-n" />
        <div className="border border-e" />
        <div className="border border-s" />
        <div className="border border-w" />
        <div className="wipe-container" data-ref="wipeContainer1">
          <img className="abs background" src={this.image('desktopawareness_120x600_background.png')} />
          <div className="wipe-container" data-ref="wipeContainer2">
            <img className="abs background-accent" src={this.image('desktopawareness_120x600_background-accent.png')} />
          </div>
        <Car
          ref={(r) => { this.cars.camry = r }}
          {...VariationAnimations['camry']}
          body={this.image(`desktopawareness_160x600_camry-body.png`)}
          wheel={this.image(`desktopawareness_160x600_camry-drive.png`)}
          rim={this.image(`desktopawareness_160x600_camry-wheel.png`)}
          lights={this.image(`desktopawareness_160x600_camry-drl.png`)}
        />
          <img className="abs event-copy" data-ref="eventCopy" src={this.image('desktopawareness_120x600_event-copy.png')} />
          <div className="event-date" data-ref="eventDate">Offers end June 4</div>
          <img className="abs toyota-logo" src={this.image('desktopawareness_120x600_toyota-logo.png')} />
          <div className="cta" data-ref="cta">Learn More</div>
          <div className="legal">
            <div className="disclosure">Options shown. Participating dealers only.</div>
          </div>
        </div>
        <LegalOverlay
          ref={(r) => { this.overlayRef = r }}
        />
      </div>
    )
  }
}

export default DesktopAwareness120x600
