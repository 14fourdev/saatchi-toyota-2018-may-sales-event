export default {
  rav4: {
    canvasWidth: 160,
    canvasHeight: 600,
    x: 8,
    y: 437,
    scale: 0.8,
    startScale: 0.65,
    distance: 200,
    yOffset: 7,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 1,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 144,
      y: 99,
      scale: 0.41,
      squash: 0.25
    },
    backWheel: {
      x: 269,
      y: 85,
      scale: 0.315,
      squash: 0.4
    }
  },
  camry: {
    canvasWidth: 160,
    canvasHeight: 600,
    x: -74,
    y: 488,
    scale: 0.95,
    startScale: 0.1,
    distance: 100,
    yOffset: -100,
    fadeDuration: 0.2,
    driveDuration: 1.9,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75
  },
  tacoma: {
    canvasWidth: 160,
    canvasHeight: 600,
    x: -68,
    y: 422,
    scale: 0.675,
    startScale: 0.4,
    distance: 210,
    yOffset: 30,
    fadeDuration: 0.3,
    driveDuration: 2,
    brakeDuration: 0.8,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 132,
      y: 94,
      scale: 0.3,
      squash: 0.23
    },
    backWheel: {
      x: 224,
      y: 76,
      scale: 0.28,
      squash: 0.6
    }
  }
}
