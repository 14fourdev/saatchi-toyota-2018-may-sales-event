export default {
  camry: {
    canvasWidth: 768,
    canvasHeight: 1024,
    retina: false,
    x: -36,
    y: 570,
    scale: 1,
    startScale: 0.4,
    distance: 350,
    yOffset: 0,
    fadeDuration: 0,
    driveDuration: 2,
    brakeDuration: 1.2,
    recoilDuration: 0.4,
    brakeStrength: 0.5,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 572,
      y: 234,
      scale: 1.25,
      squash: 0.7
    },
    backWheel: {
      x: 721,
      y: 212,
      scale: 0.85,
      squash: 0.75
    }
  }
}
