import { h } from 'preact'
import Base from './base'

if (process.env.NODE_ENV === 'development') {
  require('../toyota-config') // eslint-disable-line
}

class ToyotaStatic extends Base {
  constructor(props) {
    super(props)
    this.props.type = 'ToyotaStatic'
    this.state = { ...window.config }
  }

  componentWillMount() {
    super.componentWillMount()
    const dataUrl = [
      'https://www.buyatoyota.com/offers-service/OFFERS/allOffers/',
      `?zipCode=${this.state.zipCode}`,
      `&series=${this.props.variation}`,
      '&key=19FDC5808A3EF07D1711BAC61FF8E9BB'
    ].join('')

    const xhr = new XMLHttpRequest()
    xhr.open('GET', dataUrl)
    xhr.onreadystatechange = () => {
      if (xhr.readyState > 3 && xhr.status === 200) {
        this.setDeal(JSON.parse(xhr.response))
      }
    }
    xhr.setRequestHeader('Accept', 'application/json')
    xhr.send()
  }

  setDeal = (data) => {
    if (data.containsOffers && data.offerBundle.offers[0].lease && Object.keys(data.offerBundle.offers[0].lease).length > 0) {
      // console.log('lease', data.offerBundle.offers[0])
    } else if (data.containsOffers && data.offerBundle.offers[0].apr && Object.keys(data.offerBundle.offers[0].apr).length > 0) {
      // console.log('apr', data.offerBundle.offers[0])
    } else if (data.containsOffers && data.offerBundle.offers[0].cash && Object.keys(data.offerBundle.offers[0].cash).length > 0) {
      // console.log('cash', data.offerBundle.offers[0])
    }
  }
}

export default ToyotaStatic
