import { h, Component } from 'preact'

class Base extends Component {
  constructor(props) {
    super(props)
    this.refs = {} // eslint-disable-line
    this.imageCount = 0
    this.masterTimeline = null
  }

  componentDidMount() {
    this.setId(this.base) // set all ids in refs
    this.setupTimeline() // set up the master timeline if needed
    this.preload(this.props.images)
      .then(({ noImages }) => {
        if (this.masterTimeline) {
          this.masterTimeline.play()
        }
        if (!noImages && (this.animation === undefined || this.animation == null)) {
          console.error('You must have an animation() function in the root of your banner class')
        }
      })
  }

  componentWillUnmount() {
    if (this.masterTimeline) {
      this.masterTimeline.pause(0)
      this.masterTimeline.clear()
    }
  }

  setupTimeline = () => {
    if (TimelineLite) {
      this.masterTimeline = new TimelineLite({
        paused: true,
        onComplete: () => (this.animationComplete ? this.animationComplete() : null)
      })
      if (typeof this.animation === 'function') {
        this.animation(this.masterTimeline)
      } else if (this.animation !== undefined && this.animation !== null) {
        console.error('Your animation must be a function, not: ', this.animation)
      }
    }
  }

  setId = (el) => {
    // console.log(el.attributes.getNamedItem('data-ref'))
    // debugger // eslint-disable-line
    if (el.dataset && el.dataset.ref) {
      this.refs[el.dataset['ref']] = el // eslint-disable-line
    } else if (el.attributes && el.attributes.getNamedItem && el.attributes.getNamedItem('data-ref')) {
      this.refs[el.attributes.getNamedItem('data-ref').value] = el // eslint-disable-line
    }
    for (let i = 0; i < el.childNodes.length; i += 1) {
      this.setId(el.childNodes[i])
    }
  }

  preload = assets => new Promise((resolve) => {
    const images = assets.filter(f => !f.match(/(eot|woff|woff2|ttf)/))
    let i = 0
    const loaded = (e) => {
      i += 1
      i === images.length ? resolve({ noImages: false }) : null
    }
    const createImg = (asset) => {
      const img = new Image()
      img.onload = (e) => { loaded(e) }
      img.src = asset
    }
    if (images && !this.props.skipPreload && images.length > 0) {
      images.forEach((asset) => {
        createImg(asset)
      })
    } else {
      resolve({ noImages: true })
    }
  })

  image = (name) => {
    if (process.env.NODE_ENV === 'development') {
      return name
    } else if (process.env.STAGE === 'lamp') {
      return `/assets/img/${name.replace(/\*/g, '-')}`
    }
    return `${this.props.concept}_${this.props.size}_${this.props.variation}_${name.split('_')[3]}`
  }
}

export default Base
