import { h } from 'preact'
import Base from './base'

export default class DCM extends Base {
  constructor(props) {
    super(props)
    this.props.type = 'DCM'
  }

  componentDidMount() {
    this.setId(this.base) // set all ids in refs
    this.setupTimeline() // set up the master timeline if needed

    this.preload(this.props.images)
      .then(({ noImages }) => {
        if (window.Enabler.isInitialized()) {
          this.enablerInitHandler({ noImages })
        } else {
          window.Enabler.addEventListener(studio.events.StudioEvent.INIT, this.enablerInitHandler)
        }
      })
  }

  enablerInitHandler = ({ noImages }) => {
    if (this.animation && this.masterTimeline) {
      this.masterTimeline.play()
    } else if (!noImages && !this.animation) {
      console.error('You must have an animation() function in the root of your banner class')
    }

    if (!this.exitEvent) {
      console.error('You need to setup an exitEvent function that is called when the banner(or a button in it) is clicked.')
    }
  }
}
