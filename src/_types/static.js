import { h } from 'preact'
import Base from './base'

class Static extends Base {
  constructor(props) {
    super(props)
    this.props.type = 'Static'
  }
}

export default Static
