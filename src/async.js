import { h, Component } from 'preact'

export default class Async extends Component {
  componentWillMount = () => {
    this.cancelUpdate = false
    this.props.load.then((c) => {
      this.C = c
      if (!this.cancelUpdate) {
        this.forceUpdate()
      }
    })
  }

  componentWillUnmount = () => {
    this.cancelUpdate = true
  }

  /* eslint-disable */
  render = () => {
    const { componentProps } = this.props
    return this.C
      ? this.C.default
        ? <this.C.default {...componentProps} />
        : <this.C {...componentProps} />
      : null
  }
}
