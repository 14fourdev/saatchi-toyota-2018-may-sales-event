import Wheels from '../Wheels'
import CarKeys from '../CarKeys'

const base = {
  canvasWidth: 300,
  canvasHeight: 250,
  x: -79,
  y: 147,
  scale: 0.575,
  startScale: 0.35,
  distance: 330,
  fadeDuration: 0,
  driveDuration: 1.7,
  brakeDuration: 1.0,
  recoilDuration: 0.3,
  brakeStrength: 1,
  rimSize: 100,
  rotateSpeed: 0.75,
  invertLights: true
}

const animations = CarKeys.reduce((a, k) => {
  a[k] = { ...base, ...Wheels[k] }
  return a
}, {})

animations.tundra.y += 20
animations.tacoma.y += 30
animations.landcruiser.y += 10

export default animations
