import Wheels from '../Wheels'
import CarKeys from '../CarKeys'

const base = {
  canvasWidth: 320,
  canvasHeight: 480,
  x: -70,
  y: 517,
  scale: 0.91,
  startScale: 0.45,
  distance: 330,
  fadeDuration: 0,
  driveDuration: 1.7,
  brakeDuration: 1.0,
  recoilDuration: 0.3,
  brakeStrength: 1,
  rimSize: 100,
  rotateSpeed: 0.75,
  invertLights: true
}

const animations = CarKeys.reduce((a, k) => {
  a[k] = { ...base, ...Wheels[k] }
  return a
}, {})

export default animations
