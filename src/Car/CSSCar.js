// import { h, Component } from 'preact'
// import PropTypes from 'prop-types'
// import './CSSCar.scss'

// export default class CSSCar extends Component {
//   componentWillMount() {
//     this.rims = {}
//     this.rimSize = (this.props.rimSize || 100)
//     this.brakeStrength = this.props.brakeStrength || 1
//     this.drive = this.drive.bind(this)
//     this.brake = this.brake.bind(this)
//     this.recoil = this.recoil.bind(this)
//     if (this.props.frontWheel && this.props.backWheel) {
//       this.angle = Math.atan2(
//         this.props.backWheel.y - this.props.frontWheel.y,
//         this.props.backWheel.x - this.props.frontWheel.x
//       )
//       this.direction = (Math.cos(this.angle) > 0 ? -1 : 1)
//       this.circumference = this.rimSize * this.props.scale * this.props.frontWheel.scale * 0.5 * 2 * Math.PI
//       // const scale = (this.props.frontWheel.scale + this.props.backWheel.scale) / 2
//       // const x = (this.props.frontWheel.x + this.props.backWheel.x) / 2
//       // const y = ((this.props.frontWheel.y + this.props.backWheel.y) / 2) + ((this.rimSize / 2) * scale)
//       this.scalePosition = `${this.props.backWheel.x}px ${(this.props.backWheel.y + (this.rimSize / 2)) * this.props.backWheel.scale}px`
//     } else {
//       this.scalePosition = '50% 50%'
//     }
//   }

//   getTimeline() {
//     const brakeDuration = this.props.brakeDuration || 1.05
//     const recoilDuration = this.props.recoilDuration || 0.525
//     const driveDuration = this.props.driveDuration || 1.5
//     const fadeDuration = this.props.fadeDuration || 0
//     const driveEase = Power1.easeOut
//     const endScale = this.props.scale || 1
//     const startScale = (this.props.startScale === undefined) ? endScale : this.props.startScale
//     const yOffset = this.props.yOffset || 0
//     this.ease = { t: 0, b: 0, r: 1 }
//     this.timeline = new TimelineLite()
//     this.timeline
//       .set(this.containerRef, {
//         opacity: 0,
//         scale: endScale,
//         left: this.props.x,
//         top: this.props.y,
//         transformOrigin: `${this.scalePosition}`
//       }, 0)
//       .set(this.carBody, {
//         transformOrigin: `${this.scalePosition}`
//       })
//       .fromTo(this.containerRef, driveDuration, {
//         x: Math.cos(this.angle) * this.props.distance,
//         y: Math.sin(this.angle) * this.props.distance + yOffset,
//         scale: startScale,
//         ease: driveEase
//       }, {
//         x: 0,
//         y: 0,
//         scale: endScale,
//         ease: driveEase
//       }, 0)
//       .to(this.containerRef, fadeDuration, { opacity: 1 }, 0)
//       .to(this.ease, driveDuration, { t: 1 }, 0)
//       .add(this.drive, 0)
//       .to(this.ease, brakeDuration, { b: 1 }, driveDuration - brakeDuration)
//       .add(this.brake, driveDuration - brakeDuration)
//       .to(this.ease, recoilDuration, { r: 0, ease: Power1.easeInOut }, driveDuration)
//       .add(this.recoil, driveDuration)

//     return this.timeline
//   }

//   drive() {
//     const endScale = this.props.scale || 1
//     const startScale = (this.props.startScale === undefined) ? endScale : this.props.startScale
//     const scaleChange = (endScale - startScale) * 5
//     const loop = () => {
//       const rotation = 0.75 * this.ease.t * ((this.props.distance / this.circumference + scaleChange) * 360) * this.direction + this.ease.t
//       TweenLite.set(this.rims.front, { rotationZ: `${rotation}deg` })
//       TweenLite.set(this.rims.back, { rotationZ: `${rotation}deg` })
//       if (this.ease.t === 1) {
//         TweenLite.ticker.removeEventListener('tick', loop)
//       }
//     }
//     TweenLite.ticker.addEventListener('tick', loop)
//   }

//   brake() {
//     const loop = () => {
//       TweenLite.set(this.carBody, { rotationZ: `${this.ease.b * this.direction * this.brakeStrength}deg` })
//       if (this.ease.b === 1) {
//         TweenLite.ticker.removeEventListener('tick', loop)
//       }
//     }
//     TweenLite.ticker.addEventListener('tick', loop)
//   }

//   recoil() {
//     const loop = () => {
//       TweenLite.set(this.carBody, { rotationZ: `${(this.ease.r) * this.direction * this.brakeStrength}deg` })
//       if (this.ease.r === 0) {
//         TweenLite.ticker.removeEventListener('tick', loop)
//       }
//     }
//     TweenLite.ticker.addEventListener('tick', loop)
//   }

//   render() {
//     return (
//       <div
//         ref={(e) => { this.containerRef = e }}
//         className="car"
//       >
//         <img className="car-wheel" src={this.props.wheel} />
//         <img
//           ref={(e) => { this.carBody = e }}
//           className="car-body"
//           src={this.props.body}
//         />
//         {(this.props.frontWheel && this.props.backWheel) &&
//           <div className="car-rims">
//             <div
//               className="car-rim-wrapper"
//               style={{
//                 left: `${this.props.frontWheel.x}px`,
//                 top: `${this.props.frontWheel.y}px`,
//                 transform: `translate(-50%, -50%) scale(${this.props.frontWheel.scale}) rotateY(${90 * this.props.frontWheel.squash}deg)`
//               }}
//             >
//               <img
//                 ref={(e) => { this.rims.front = e }}
//                 className="car-rim"
//                 src={this.props.rim}
//               />
//             </div>
//             <div
//               className="car-rim-wrapper"
//               style={{
//                 left: `${this.props.backWheel.x}px`,
//                 top: `${this.props.backWheel.y}px`,
//                 transform: `translate(-50%, -50%) scale(${this.props.backWheel.scale}) rotateY(${90 * this.props.backWheel.squash}deg)`
//               }}
//             >
//               <img
//                 ref={(e) => { this.rims.back = e }}
//                 className="car-rim"
//                 src={this.props.rim}
//               />
//             </div>
//           </div>
//         }
//       </div>
//     )
//   }
// }

// const wheelPropShape = PropTypes.shape({
//   x: PropTypes.number.isRequired,
//   y: PropTypes.number.isRequired,
//   scale: PropTypes.number.isRequired,
//   squash: PropTypes.number.isRequired
// })

// CSSCar.propTypes = {
//   x: PropTypes.number.isRequired,
//   y: PropTypes.number.isRequired,
//   yOffset: PropTypes.number,
//   scale: PropTypes.number,
//   startScale: PropTypes.number,
//   distance: PropTypes.number.isRequired,
//   fadeDuration: PropTypes.number,
//   driveDuration: PropTypes.number,
//   brakeDuration: PropTypes.number,
//   recoilDuration: PropTypes.number,
//   body: PropTypes.string.isRequired,
//   wheel: PropTypes.string,
//   rim: PropTypes.string,
//   rimSize: PropTypes.number,
//   frontWheel: wheelPropShape,
//   backWheel: wheelPropShape
// }