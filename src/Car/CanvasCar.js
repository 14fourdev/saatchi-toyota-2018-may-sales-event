// to add DRL, just pass in a 'lights' prop with the DRL image, just like the other image props
import { h, Component } from 'preact'
import PropTypes from 'prop-types'
import './CanvasCar.scss'

export default class CanvasCar extends Component {
  componentWillMount() {
    if (this.props.retina !== undefined) {
      this.retinaScale = this.props.retina ? 2 : 1
    } else {
      this.retinaScale = 2
    }
    this.frontFacing = !(this.props.backWheel && this.props.frontWheel)
    this.animateProps = {}
    this.carBody = {}
    this.rims = {
      front: {},
      back: {}
    }
    this.rimSize = (this.props.rimSize || 100)
    this.brakeStrength = this.props.brakeStrength || 1
    this.renderAnimation = this.renderAnimation.bind(this)
    if (!this.frontFacing) {
      this.angle = Math.atan2(
        this.props.backWheel.y - this.props.frontWheel.y,
        this.props.backWheel.x - this.props.frontWheel.x
      )
      this.direction = (Math.cos(this.angle) > 0 ? -1 : 1)
      this.circumference = this.rimSize * this.props.scale * this.props.frontWheel.scale * 0.5 * 2 * Math.PI
      this.scalePosition = `${this.props.backWheel.x}px ${(this.props.backWheel.y + (this.rimSize / 2)) * this.props.backWheel.scale}px`
    } else {
      this.scalePosition = '50% 50%'
      this.angle = -Math.PI / 2
      this.direction = 1
    }
  }

  componentDidMount() {
    this.canvas.width = (this.props.canvasWidth || document.querySelector('.banner').getBoundingClientRect().width) * this.retinaScale
    this.canvas.height = (this.props.canvasHeight || document.querySelector('.banner').getBoundingClientRect().height) * this.retinaScale
    this.canvas.style.width = `${this.canvas.width / this.retinaScale}px`
    this.canvas.style.height = `${this.canvas.height / this.retinaScale}px`
    this.context = this.canvas.getContext('2d')
    this.images = {
      body: new Image(),
      wheel: new Image(),
      rim: new Image(),
      lights: new Image()
    }
    this.images.body.src = this.props.body
    this.images.wheel.src = this.props.wheel
    this.images.rim.src = this.props.rim
    if (this.props.lights) {
      this.images.lights.src = this.props.lights
    }
  }

  getTimeline() {
    const brakeDuration = this.props.brakeDuration || 1.05
    const recoilDuration = this.props.recoilDuration || 0.525
    const driveDuration = this.props.driveDuration || 1.5
    const fadeDuration = this.props.fadeDuration || 0
    const driveEase = Power1.easeOut
    const endScale = this.props.scale || 1
    const startScale = (this.props.startScale === undefined) ? endScale : this.props.startScale
    const yOffset = this.props.yOffset || 0
    this.ease = { drive: 0, brake: 0, recoil: 1, lights: 1 }
    this.timeline = new TimelineLite()
      .add(() => { TweenLite.ticker.addEventListener('tick', this.renderAnimation) }, 0)
      .set(this.animateProps, {
        opacity: 0,
        scale: endScale,
        left: this.props.x,
        top: this.props.y,
        transformOrigin: `${this.scalePosition}`
      }, 0)
      .set(this.carBody, {
        rotationZ: 0,
        transformOrigin: `${this.scalePosition}`
      })
      .fromTo(this.animateProps, driveDuration, {
        x: Math.cos(this.angle) * this.props.distance,
        y: Math.sin(this.angle) * this.props.distance + yOffset,
        scale: startScale,
        ease: driveEase
      }, {
        x: 0,
        y: 0,
        scale: endScale,
        ease: driveEase
      }, 0)
      .to(this.animateProps, fadeDuration, { opacity: 1 }, 0)
      .to(this.ease, driveDuration, { drive: 1 }, 0)
      .add(() => { this.driving = true }, 0)
      .to(this.ease, brakeDuration, { brake: 1 }, driveDuration - brakeDuration)
      .add(() => { this.braking = true }, driveDuration - brakeDuration)
      .to(this.ease, recoilDuration, { recoil: 0, ease: Power1.easeInOut }, driveDuration)
      .to(this.ease, 0.5, { lights: 0, ease: Power1.easeInOut })
      .add(() => { this.recoiling = true }, driveDuration)
      .add(() => { TweenLite.ticker.removeEventListener('tick', this.renderAnimation) })

    return this.timeline
  }

  drawCar() {
    let anchorX = this.frontFacing ? this.images.body.width / 2 : this.props.backWheel.x
    let anchorY = this.frontFacing ? this.images.body.height / 2 : this.props.backWheel.y
    // reset transform for draw
    this.context.setTransform(1, 0, 0, 1, 0, 0)

    // clear canvas
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)

    // apply fade alpha -- need to change this to the canvas element itself in the above timeline
    this.context.globalAlpha = this.animateProps.opacity

    // car drive
    this.context.translate(this.animateProps.x + this.props.x, this.animateProps.y + this.props.y)
    this.context.translate(anchorX, anchorY)
    this.context.scale(this.animateProps.scale, this.animateProps.scale)
    this.context.translate(-anchorX, -anchorY)
    this.context.drawImage(this.images.wheel, 0, 0)

    // car body
    this.context.save()
    this.context.translate(anchorX, anchorY)
      if (!this.frontFacing) {
        this.context.rotate(this.carBody.rotationZ)
      }
      this.context.drawImage(this.images.body, -anchorX, -anchorY + (this.frontFacing ? (this.carBody.rotationZ * 90) : 0))
      // lights
      if (this.props.lights) {
        const lightEase = this.props.invertLights ? (1 - this.ease.lights) : this.ease.lights
        this.context.globalAlpha = lightEase
        this.context.drawImage(this.images.lights, -anchorX, -anchorY + (this.frontFacing ? (this.carBody.rotationZ * 90) : 0))
      }
    this.context.restore()

    if (!this.frontFacing) {
      // back rim
      this.context.translate(this.props.backWheel.x, this.props.backWheel.y)
      this.context.save()
        this.context.scale(
          this.props.backWheel.scale * (1 - this.props.backWheel.squash),
          this.props.backWheel.scale
        )
        this.context.rotate(this.rims.front.rotationZ)
        this.context.drawImage(this.images.rim, -this.images.rim.width / 2, -this.images.rim.height / 2)
      this.context.restore()
      this.context.translate(-this.props.backWheel.x, -this.props.backWheel.y)
      
      // front rim
      this.context.translate(this.props.frontWheel.x, this.props.frontWheel.y)
      this.context.save()
        this.context.scale(
          this.props.frontWheel.scale * (1 - this.props.frontWheel.squash),
          this.props.frontWheel.scale
        )
        this.context.rotate(this.rims.back.rotationZ)
        this.context.drawImage(this.images.rim, -this.images.rim.width / 2, -this.images.rim.height / 2)
      this.context.restore()
      this.context.translate(-this.props.frontWheel.x, -this.props.frontWheel.y)
    }
  }

  renderAnimation() {
    if (this.driving) {
      if (!this.frontFacing) {
        const endScale = this.props.scale || 1
        const startScale = (this.props.startScale === undefined) ? (this.props.scale || 1) : this.props.startScale
        const rotateSpeed = (this.props.rotateSpeed === undefined) ? 1 : this.props.rotateSpeed
        const scaleChange = (endScale - startScale) * 5
        const rotation = (0.75 * this.ease.drive * ((this.props.distance / this.circumference + scaleChange) * 360) * this.direction + this.ease.drive) * (Math.PI / 180) * rotateSpeed
        TweenLite.set(this.rims.front, { rotationZ: rotation })
        TweenLite.set(this.rims.back, { rotationZ: rotation })
      }
      this.drive = this.ease.drive !== 1
    }

    if (this.braking) {
      TweenLite.set(this.carBody, { rotationZ: this.ease.brake * this.direction * this.brakeStrength * (Math.PI / 180) })
      this.braking = this.ease.brake !== 1
    }

    if (this.recoiling) {
      TweenLite.set(this.carBody, { rotationZ: (this.ease.recoil) * this.direction * this.brakeStrength * (Math.PI / 180) })
      this.recoiling = this.ease.recoil !== 0
    }

    this.drawCar()
  }

  render() {
    return (
      <div className="car">
        <canvas ref={(r) => { this.canvas = r }}></canvas>
      </div>
    )
  }
}

const wheelPropShape = PropTypes.shape({
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  scale: PropTypes.number.isRequired,
  squash: PropTypes.number.isRequired
})

CanvasCar.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  yOffset: PropTypes.number,
  scale: PropTypes.number,
  startScale: PropTypes.number,
  distance: PropTypes.number.isRequired,
  fadeDuration: PropTypes.number,
  driveDuration: PropTypes.number,
  brakeDuration: PropTypes.number,
  recoilDuration: PropTypes.number,
  body: PropTypes.string.isRequired,
  wheel: PropTypes.string,
  rim: PropTypes.string,
  lights: PropTypes.string,
  rimSize: PropTypes.number,
  rotateSpeed: PropTypes.number,
  frontWheel: wheelPropShape,
  backWheel: wheelPropShape
}
