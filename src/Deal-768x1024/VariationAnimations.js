import Wheels from '../Wheels'
import CarKeys from '../CarKeys'

const base = {
  canvasWidth: 768,
  canvasHeight: 1024,
  retina: false,
  x: 38,
  y: 580,
  scale: 1.07,
  startScale: 0.45,
  distance: 630,
  fadeDuration: 0,
  driveDuration: 1.7,
  brakeDuration: 1.0,
  recoilDuration: 0.3,
  brakeStrength: 1,
  rimSize: 100,
  rotateSpeed: 0.75,
  invertLights: true
}

const animations = CarKeys.reduce((a, k) => {
  a[k] = { ...base, ...Wheels[k] }
  return a
}, {})

export default animations
