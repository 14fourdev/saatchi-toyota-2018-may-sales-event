import { h, Component, render } from 'preact'
import { Deal as BannerClass } from '../banner-config'
import VariationAnimations from './VariationAnimations'
import Car from '../Car/CanvasCar'
import LegalOverlay from '../LegalOverlay/LegalOverlay'
import './Deal-768x1024.scss'

class Deal768x1024 extends BannerClass {
  componentWillMount() {
    this.delayShowLegal = this.delayShowLegal.bind(this)
    this.interruptShowLegal = this.interruptShowLegal.bind(this)
    this.showLegal = this.showLegal.bind(this)
  }

  animation(timeline) {
    timeline
      .to(this.refs.banner, 0.3, { autoAlpha: 1 })
      .to(this.refs.wipeContainer1, 0.8, { height: '100%', ease: Power1.easeIn })
      .to(this.refs.wipeContainer2, 0.8, { height: '100%', ease: Power0.easeNone }, 0.5)
      .from(this.refs.eventDate, 0.7, { y: -400, ease: Power1.easeOut }, 1.3)
      .from(this.refs.eventCopy, 0.7, { y: '-100%', ease: Power1.easeOut }, 1.3)
      .add(this.carRef.getTimeline(), 1.3)
      .from(this.refs.separator, 0.7, { y: -350, ease: Power1.easeOut }, 1.3)
  }

  animationComplete() {
    // animation finished
  }

  delayShowLegal() {
    this.timer = setTimeout(this.showLegal, 750)
  }

  interruptShowLegal() {
    clearTimeout(this.timer)
  }

  showLegal() {
    this.overlayRef.show()
  }

  render() {
    return (
      <div className="banner Deal-768x1024 hidden" data-ref="banner">
        <div className="border border-n" />
        <div className="border border-e" />
        <div className="border border-s" />
        <div className="border border-w" />
        <div className="wipe-container" data-ref="wipeContainer1">
          <img className="abs background" src={this.image('deal_768x1024_*_background.png')} />
          <div className="wipe-container" data-ref="wipeContainer2">
            <img className="abs background-accent" src={this.image('deal_768x1024_*_background-accent.png')} />
          </div>
          <Car
            ref={(r) => { this.carRef = r }}
            {...VariationAnimations[this.props.variation]}
            body={this.image(`deal_768x1024_${this.props.variation}_body.png`)}
            wheel={this.image(`deal_768x1024_${this.props.variation}_wheel.png`)}
            lights={this.props.variation.indexOf('rav4') >= 0 ? null : this.image(`deal_768x1024_${this.props.variation}_lights.png`)}
            rim={this.image(`deal_*_${this.props.variation}_rim.png`)}
          />
          <img className="abs event-copy" data-ref="eventCopy" src={this.image('deal_768x1024_*_event-copy.png')} />
          <div className="event-date" data-ref="eventDate">Offers end June 4</div>
          <img className="abs toyota-logo" src={this.image('deal_768x1024_*_toyota-logo.png')} />
          <div className="cta-container">
            <div className="cta" data-ref="cta">Learn More</div>
          </div>
          <div className="legal" data-ref="legal" onClick={this.showLegal} onMouseOver={this.delayShowLegal} onMouseOut={this.interruptShowLegal}>
            <div className="disclosure">Options shown. Participating dealers only.<br/>[*]Important Offer Info</div>
          </div>
        </div>
        <div className="apr-container">
          <div className="apr-wrapper">
            <div className="apr-percent">0%</div>
            <div className="apr-copy">APR<span>*</span></div>
            <div className="apr-sub">on the 2018 RAV4</div>
          </div>
        </div>
        <LegalOverlay
          ref={(r) => { this.overlayRef = r }}
        />
        <div className="separator" data-ref="separator" />
      </div>
    )
  }
}

export default Deal768x1024
