import CarKeys from '../CarKeys'

const base = {
  canvasWidth: 160,
  canvasHeight: 600,
  x: 5,
  y: 427,
  distance: 100,
  scale: 1.44,
  startScale: 0.5,
  fadeDuration: 0.2,
  driveDuration: 1.7,
  brakeDuration: 1.0,
  recoilDuration: 0.3,
  brakeStrength: 1,
  invertLights: true
}

const animations = CarKeys.reduce((a, k) => {
  a[k] = { ...base }
  return a
}, {})

export default animations
