// '4runner',
// '86',
// 'avalon',
// 'avalon-hybrid',
// 'camry',
// 'camry-hybrid',
// 'corolla',
// 'corolla-im',
// 'highlander',
// 'highlander-hybrid',
// 'landcruiser',
// 'prius',
// 'prius-prime',
// 'prius-c',
// 'prius-v',
// 'rav4',
// 'rav4-hybrid',
// 'sequoia',
// 'sienna',
// 'tacoma',
// 'tundra',
// 'yaris',
// 'yaris-ia'

export default {
  '4runner': {
    frontWheel: {
      x: 346,
      y: 240,
      scale: 1.3,
      squash: 0.35
    },
    backWheel: {
      x: 612,
      y: 209,
      scale: 1.1,
      squash: 0.5
    }
  },
  '86': {
    frontWheel: {
      x: 396,
      y: 239,
      scale: 1.35,
      squash: 0.4
    },
    backWheel: {
      x: 651,
      y: 213,
      scale: 1.05,
      squash: 0.6
    }
  },
  'avalon': {
    frontWheel: {
      x: 350,
      y: 235,
      scale: 1.375,
      squash: 0.3
    },
    backWheel: {
      x: 631,
      y: 219,
      scale: 1.025,
      squash: 0.45
    }
  },
  'avalon-hybrid': {
    frontWheel: {
      x: 351,
      y: 235,
      scale: 1.375,
      squash: 0.3
    },
    backWheel: {
      x: 633,
      y: 219,
      scale: 1.025,
      squash: 0.45
    }
  },
  'camry': {
    frontWheel: {
      x: 352,
      y: 236,
      scale: 0.985,
      squash: 0.28
    },
    backWheel: {
      x: 637,
      y: 217,
      scale: 0.75,
      squash: 0.45
    }
  },
  'camry-hybrid': {
    frontWheel: {
      x: 350,
      y: 237,
      scale: 0.945,
      squash: 0.315
    },
    backWheel: {
      x: 633,
      y: 217,
      scale: 0.7,
      squash: 0.5
    }
  },
  'corolla': {
    frontWheel: {
      x: 367,
      y: 232,
      scale: 1.3,
      squash: 0.3
    },
    backWheel: {
      x: 642,
      y: 211,
      scale: 1.05,
      squash: 0.5
    }
  },
  'corolla-im': {
    frontWheel: {
      x: 384,
      y: 237,
      scale: 1.25,
      squash: 0.375
    },
    backWheel: {
      x: 644,
      y: 218,
      scale: 1,
      squash: 0.525
    }
  },
  'highlander': {
    frontWheel: {
      x: 371,
      y: 244,
      scale: 1.35,
      squash: 0.325
    },
    backWheel: {
      x: 632,
      y: 212.75,
      scale: 1.075,
      squash: 0.5
    }
  },
  'highlander-hybrid': {
    frontWheel: {
      x: 372,
      y: 243,
      scale: 1.4,
      squash: 0.325
    },
    backWheel: {
      x: 632,
      y: 211,
      scale: 1.075,
      squash: 0.5
    }
  },
  'landcruiser': {
    frontWheel: {
      x: 350,
      y: 239,
      scale: 1.2,
      squash: 0.35
    },
    backWheel: {
      x: 611,
      y: 216,
      scale: 1,
      squash: 0.5
    }
  },
  'prius': {
    frontWheel: {
      x: 354,
      y: 234,
      scale: 1.25,
      squash: 0.3
    },
    backWheel: {
      x: 637,
      y: 220,
      scale: 1.0,
      squash: 0.5
    }
  },
  'prius-prime': {
    frontWheel: {
      x: 352,
      y: 232,
      scale: 1.3,
      squash: 0.3
    },
    backWheel: {
      x: 636,
      y: 218,
      scale: 1.05,
      squash: 0.5
    }
  },
  'prius-c': {
    frontWheel: {
      x: 365,
      y: 241,
      scale: 0.9,
      squash: 0.35
    },
    backWheel: {
      x: 650,
      y: 205,
      scale: 0.675,
      squash: 0.5
    }
  },
  'prius-v': {
    frontWheel: {
      x: 350,
      y: 242,
      scale: 1.35,
      squash: 0.3
    },
    backWheel: {
      x: 629,
      y: 214,
      scale: 1.1,
      squash: 0.45
    }
  },
  'rav4': {
    frontWheel: {
      x: 343,
      y: 246,
      scale: 1,
      squash: 0.26
    },
    backWheel: {
      x: 635,
      y: 213,
      scale: 0.75,
      squash: 0.4
    }
  },
  'rav4-hybrid': {
    frontWheel: {
      x: 346,
      y: 246,
      scale: 1.2,
      squash: 0.26
    },
    backWheel: {
      x: 630,
      y: 213,
      scale: 0.95,
      squash: 0.4
    }
  },
  'sequoia': {
    frontWheel: {
      x: 358,
      y: 255,
      scale: 1,
      squash: 0.35
    },
    backWheel: {
      x: 633,
      y: 208,
      scale: 0.8,
      squash: 0.55
    }
  },
  'sienna': {
    frontWheel: {
      x: 346,
      y: 237,
      scale: 0.9,
      squash: 0.285
    },
    backWheel: {
      x: 629,
      y: 211,
      scale: 0.7,
      squash: 0.45
    }
  },
  'tacoma': {
    frontWheel: {
      x: 313,
      y: 248,
      scale: 0.75,
      squash: 0.35
    },
    backWheel: {
      x: 612,
      y: 202,
      scale: 0.65,
      squash: 0.45
    }
  },
  'tundra': {
    frontWheel: {
      x: 323,
      y: 243,
      scale: 0.825,
      squash: 0.375
    },
    backWheel: {
      x: 620,
      y: 197,
      scale: 0.65,
      squash: 0.525
    }
  },
  'yaris': {
    frontWheel: {
      x: 392,
      y: 237,
      scale: 1.45,
      squash: 0.4
    },
    backWheel: {
      x: 648,
      y: 218,
      scale: 1.15,
      squash: 0.55
    }
  },
  'yaris-ia': {
    frontWheel: {
      x: 363,
      y: 239,
      scale: 1.35,
      squash: 0.35
    },
    backWheel: {
      x: 637,
      y: 217,
      scale: 1.05,
      squash: 0.55
    }
  }
}
