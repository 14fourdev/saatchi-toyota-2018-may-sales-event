export default {
  camry: {
    canvasWidth: 320,
    canvasHeight: 90,
    x: 212,
    y: -33,
    scale: 0.5 * 1.075,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 213,
      y: 188,
      scale: 0.91,
      squash: 0.65
    },
    backWheel: {
      x: 79,
      y: 147.5,
      scale: 0.75,
      squash: 0.75
    }
  },
  rav4: {
    canvasWidth: 320,
    canvasHeight: 90,
    x: -110,
    y: -35,
    scale: 0.5 * 1.075,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 226,
      y: 155,
      scale: 0.65,
      squash: 0.25
    },
    backWheel: {
      x: 422,
      y: 133,
      scale: 0.5,
      squash: 0.45
    }
  },
  corolla: {
    canvasWidth: 320,
    canvasHeight: 90,
    x: 396,
    y: -6,
    scale: 0.5 * 1.075,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 198,
      y: 114,
      scale: 0.8,
      squash: 0.35
    },
    backWheel: {
      x: 34,
      y: 102,
      scale: 0.6,
      squash: 0.45
    }
  },
  highlander: {
    canvasWidth: 320,
    canvasHeight: 90,
    x: -165,
    y: -25,
    scale: 0.5 * 1.075,
    startScale: 0.2,
    distance: 150,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 203.5,
      y: 127,
      scale: 0.8,
      squash: 0.365
    },
    backWheel: {
      x: 353,
      y: 109,
      scale: 0.6,
      squash: 0.55
    }
  }
}
