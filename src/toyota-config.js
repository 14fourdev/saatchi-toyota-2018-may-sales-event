
window.config = {
  // Description: This is the hard coded zip code to use in the deal banners.
  // Formatting: Just the zip code is needed without single quotes, followed by a comma.
  // Applicable Banners: Zip, Deal-lease, Deal-cash, Deal-apr
  // Applicable Sizes: All sizes
  // Example line: zipCode: 99201,
  zipCode: 99201,

  // Description: This is the text that is on the button for all of the banners but the zip banner.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Default, Deal-static, Deal-lease, Deal-cash, Deal-apr
  // Applicable Sizes: All sizes
  // Example line: cta: 'Offer Details',
  cta: 'Offer Details',

  // Description: This is the text that is on the button on the 160x600 and 728x90 zip banners.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Zip
  // Applicable Sizes: 160x600, 728x90
  // Example line: zipCta: 'Local Offers',
  zipCta: 'Local Offers',

  // Description: This is the text that is on the button on the 300x250 zip banner.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Zip
  // Applicable Sizes: 300x250
  // Example line: zipCtaLong: 'See Local Offers',
  zipCtaLong: 'See Local Offers',

  // Description: This is the deal text that is used on the deal-static banners.
  //              If no text is provided, it will use the dealCopy text for the deal-static banner.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Deal-static
  // Applicable Sizes: All sizes
  // Example line: hardCodedDealCopy: 'This is the customizable deal copy for a static deal unit.',
  hardCodedDealCopy: 'This is the customizable deal copy for a static deal unit.',

  // Description: This is the URL that the button on the deal-static banner will link to.
  //              If no URL is provided, it will use the exitURL for the button link.
  // Formatting: The link should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Deal-static
  // Applicable Sizes: All sizes
  // Example line: hardCodedDealURL: 'http://www.toyota.com/',
  hardCodedDealURL: 'http://www.toyota.com/',

  // Description: This is the text that is used on deal banners when no offers are returned for the given car/zipcode.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Deal-lease, Deal-cash, Deal-apr
  // Applicable Sizes: All sizes
  // Example line: dealCopy: 'Great deals available at your local Toyota dealer.',
  dealCopy: 'Great deals available at your local Toyota dealer.',

  // Description: This is the URL that the button on the default banner will link to.
  // Formatting: The link should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: Default
  // Applicable Sizes: All sizes
  // Example line: exitURL: 'http://www.toyota.com/local-specials',
  exitURL: 'http://www.toyota.com/local-specials',

  // Description: This is the opening headline text on all of the banners.
  // Formatting: The text should be surrounded by single quotes (''), followed by a comma.
  // Applicable Banners: All banners
  // Applicable Sizes: All sizes
  // Example line: campaignCopy: 'Save on 2018 Toyotas',
  campaignCopy: 'Save on 2018 Toyotas',

  // Description: This is the text that is on the ending screen under the sales event logo.
  // Formatting: The text should be surrounded by single quotes (''). NO COMMA AT THE END OF THIS ONE!
  // Applicable Banners: All banners
  // Applicable Sizes: All sizes
  // Example line: endDateCopy: 'Before they’re all gone'
  endDateCopy: 'Before they’re all gone'
}
