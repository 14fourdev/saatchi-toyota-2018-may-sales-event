import { h, Component } from 'preact'
import './LegalOverlay.scss'

export default class LegalOverlay extends Component {
  componentWillMount() {
    this.hide = this.hide.bind(this)
  }

  show() {
    this.el.classList.add('visible')
  }

  hide() {
    this.el.classList.remove('visible')
  }

  render() {
    return (
      <div ref={(e) => { this.el = e }} className="legal-overlay" onClick={this.hide}>
        <p>1. 0.9% Annual Percentage Rates (APR) for 36 months, 0.9% APR for 48 months, 0.9% APR for 60, 2.9% APR for 72 months available to eligible customers who finance a new, unused, or unlicensed 2018 Camry from Toyota Motor Sales, USA, Inc. (TMS) and Toyota Financial Services (TFS). Offer valid 04-03-2018 through 04-30-2018. Specific vehicles are subject to availability. You must take retail delivery from dealer stock. Special APR may not be combined with any other Customer Cash Rebates, Bonus Cash Rebates, or Lease Offers. Finance programs available on credit approval. Not all buyers will qualify for financing from Toyota Financial Services through participating dealers. Monthly payment for every $1,000 financed is 0.9% - 36 months = $28.16; 0.9% - 48 months = $21.22; 0.9% - 60 months = $17.05; 2.9% - 72 months = $15.15. See your Toyota dealer for actual pricing, annual percentage rate (APR), monthly payment, and other terms and special offers. Pricing and terms of any finance or lease transaction will be agreed upon by you and your dealer. Special offers are subject to change or termination at any time.</p>
        <div className="legal-overlay-close">X</div>
      </div>
    )
  }
}
