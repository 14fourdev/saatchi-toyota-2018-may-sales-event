export default {
  rav4: {
    canvasWidth: 120,
    canvasHeight: 600,
    x: -53,
    y: 438,
    scale: 0.68,
    startScale: 0.45,
    distance: 200,
    yOffset: 7,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 1,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 144,
      y: 99,
      scale: 0.41,
      squash: 0.25
    },
    backWheel: {
      x: 269,
      y: 85,
      scale: 0.315,
      squash: 0.4
    }
  },
  camry: {
    canvasWidth: 120,
    canvasHeight: 600,
    x: -150,
    y: 444,
    scale: 0.75,
    startScale: 0.1,
    distance: 100,
    yOffset: -50,
    fadeDuration: 0.2,
    driveDuration: 1.9,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75
  },
  tacoma: {
    canvasWidth: 120,
    canvasHeight: 600,
    x: -98,
    y: 422,
    scale: 0.53,
    startScale: 0.3,
    distance: 210,
    yOffset: 30,
    fadeDuration: 0.3,
    driveDuration: 2,
    brakeDuration: 0.8,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 132,
      y: 94,
      scale: 0.3,
      squash: 0.23
    },
    backWheel: {
      x: 224,
      y: 76,
      scale: 0.28,
      squash: 0.6
    }
  }
}
