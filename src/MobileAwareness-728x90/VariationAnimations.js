export default {
  rav4: {
    canvasWidth: 728,
    canvasHeight: 90,
    x: 508,
    y: 10,
    scale: 0.9,
    startScale: 0.65,
    distance: 120,
    yOffset: 7,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 1,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 144,
      y: 99,
      scale: 0.41,
      squash: 0.25
    },
    backWheel: {
      x: 269,
      y: 85,
      scale: 0.315,
      squash: 0.4
    }
  },
  camry: {
    canvasWidth: 728,
    canvasHeight: 90,
    x: 591,
    y: 20,
    scale: 0.97,
    startScale: 0.3,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 270,
      y: 111,
      scale: 0.6,
      squash: 0.75
    },
    backWheel: {
      x: 341,
      y: 100,
      scale: 0.4,
      squash: 0.8
    }
  },
  tacoma: {
    canvasWidth: 728,
    canvasHeight: 90,
    x: 868,
    y: 13,
    scale: 0.9,
    startScale: 0.7,
    distance: 120,
    yOffset: 20,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 122,
      y: 94,
      scale: 0.3,
      squash: 0.28
    },
    backWheel: {
      x: 31,
      y: 77,
      scale: 0.28,
      squash: 0.6
    }
  }
}
