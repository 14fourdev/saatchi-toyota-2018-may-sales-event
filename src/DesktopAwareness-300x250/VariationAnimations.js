export default {
  camry: {
    canvasWidth: 300,
    canvasHeight: 250,
    x: 200,
    y: 200,
    scale: 0.5,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 213,
      y: 188,
      scale: 0.91,
      squash: 0.65
    },
    backWheel: {
      x: 79,
      y: 147.5,
      scale: 0.75,
      squash: 0.75
    }
  },
  rav4: {
    canvasWidth: 300,
    canvasHeight: 250,
    x: -120,
    y: 200,
    scale: 0.5,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 226,
      y: 155,
      scale: 0.65,
      squash: 0.25
    },
    backWheel: {
      x: 422,
      y: 133,
      scale: 0.5,
      squash: 0.45
    }
  },
  corolla: {
    canvasWidth: 300,
    canvasHeight: 250,
    x: 374,
    y: 227,
    scale: 0.5,
    startScale: 0.2,
    distance: 100,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 198,
      y: 114,
      scale: 0.8,
      squash: 0.35
    },
    backWheel: {
      x: 34,
      y: 102,
      scale: 0.6,
      squash: 0.45
    }
  },
  highlander: {
    canvasWidth: 300,
    canvasHeight: 250,
    x: -175,
    y: 210,
    scale: 0.5,
    startScale: 0.2,
    distance: 150,
    yOffset: 0,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 203.5,
      y: 127,
      scale: 0.8,
      squash: 0.365
    },
    backWheel: {
      x: 353,
      y: 109,
      scale: 0.6,
      squash: 0.55
    }
  }
}
