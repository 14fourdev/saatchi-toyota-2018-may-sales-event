import { h, Component, render } from 'preact'
import { DesktopAwareness as BannerClass } from '../banner-config'
import VariationAnimations from './VariationAnimations'
import Car from '../Car/CanvasCar'
import LegalOverlay from '../LegalOverlay/LegalOverlay'
import './DesktopAwareness-300x250.scss'

class DesktopAwareness300x250 extends BannerClass {
  componentWillMount() {
    this.cars = {}
    this.delayShowLegal = this.delayShowLegal.bind(this)
    this.interruptShowLegal = this.interruptShowLegal.bind(this)
    this.showLegal = this.showLegal.bind(this)
  }

  animation(timeline) {
    timeline
      .to(this.refs.banner, 0.3, { autoAlpha: 1 })
      .to(this.refs.wipeContainer1, 0.65, { height: '100%', ease: Power1.easeIn })
      .to(this.refs.wipeContainer2, 0.65, { height: '100%', ease: Power1.easeIn }, 0.7)
      .from(this.refs.eventDate, 0.7, { y: -200, ease: Power1.easeOut }, 1.25)
      .from(this.refs.eventCopy, 0.7, { y: '-100%', ease: Power1.easeOut }, 1.3)
      .add(this.cars.camry.getTimeline(), 1.5)
      .add(this.cars.rav4.getTimeline(), 2.3)
      .add(this.cars.corolla.getTimeline(), 2.7)
      .add(this.cars.highlander.getTimeline(), 3)
  }

  animationComplete() {
    // animation finished
  }

  delayShowLegal() {
    this.timer = setTimeout(this.showLegal, 750)
  }

  interruptShowLegal() {
    clearTimeout(this.timer)
  }

  showLegal() {
    this.overlayRef.show()
  }

  render() {
    return (
      <div className="banner DesktopAwareness-300x250 hidden" data-ref="banner">
        <div className="border border-n" />
        <div className="border border-e" />
        <div className="border border-s" />
        <div className="border border-w" />
        <div className="wipe-container" data-ref="wipeContainer1">
          <img className="abs background" src={this.image('desktopawareness_300x250_background.png')} />
          <div className="wipe-container" data-ref="wipeContainer2">
            <img className="abs background-accent" src={this.image('desktopawareness_300x250_background-accent.png')} />
          </div>
        <Car
          ref={(r) => { this.cars.highlander = r }}
          {...VariationAnimations['highlander']}
          body={this.image(`desktopawareness_300x250_highlander-body.png`)}
          wheel={this.image(`desktopawareness_300x250_highlander-drive.png`)}
          rim={this.image(`desktopawareness_300x250_highlander-wheel.png`)}
          lights={this.image(`desktopawareness_300x250_highlander-drl.png`)}
        />
        <Car
          ref={(r) => { this.cars.corolla = r }}
          {...VariationAnimations['corolla']}
          body={this.image(`desktopawareness_300x250_corolla-body.png`)}
          wheel={this.image(`desktopawareness_300x250_corolla-drive.png`)}
          rim={this.image(`desktopawareness_300x250_corolla-wheel.png`)}
          lights={this.image(`desktopawareness_300x250_corolla-drl.png`)}
        />
        <Car
          ref={(r) => { this.cars.rav4 = r }}
          {...VariationAnimations['rav4']}
          body={this.image(`desktopawareness_300x250_rav4-body.png`)}
          wheel={this.image(`desktopawareness_300x250_rav4-drive.png`)}
          rim={this.image(`desktopawareness_300x250_rav4-wheel.png`)}
        />
        <Car
          ref={(r) => { this.cars.camry = r }}
          {...VariationAnimations['camry']}
          body={this.image(`desktopawareness_300x250_camry-body.png`)}
          wheel={this.image(`desktopawareness_300x250_camry-drive.png`)}
          rim={this.image(`desktopawareness_300x250_camry-wheel.png`)}
          lights={this.image(`desktopawareness_300x250_camry-drl.png`)}
        />
          <img className="abs event-copy" data-ref="eventCopy" src={this.image('desktopawareness_300x250_event-copy.png')} />
          <div className="event-date" data-ref="eventDate">Offers end June 4</div>
          <img className="abs toyota-logo" src={this.image('desktopawareness_300x250_toyota-logo.png')} />
          <div className="cta" data-ref="cta">Learn More</div>
          <div className="legal">
            <div className="disclosure">Options shown. Participating dealers only.</div>
          </div>
        </div>
        <LegalOverlay
          ref={(r) => { this.overlayRef = r }}
        />
      </div>
    )
  }
}

export default DesktopAwareness300x250
