import Wheels from '../Wheels'
import CarKeys from '../CarKeys'

const base = {
  canvasWidth: 728,
  canvasHeight: 90,
  x: 414,
  y: -100,
  scale: 0.48,
  startScale: 0.25,
  distance: 330,
  fadeDuration: 0.4,
  driveDuration: 1.7,
  brakeDuration: 1.0,
  recoilDuration: 0.3,
  brakeStrength: 1,
  rimSize: 100,
  rotateSpeed: 0.75,
  invertLights: true
}

const animations = CarKeys.reduce((a, k) => {
  a[k] = { ...base, ...Wheels[k] }
  return a
}, {})

animations.tundra.y += 10
animations.tacoma.y += 10
animations.landcruiser.y += 3

export default animations
