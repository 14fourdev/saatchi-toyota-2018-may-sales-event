export default {
  rav4: {
    canvasWidth: 320,
    canvasHeight: 480,
    x: 22,
    y: 516,
    scale: 0.875,
    startScale: 0.3,
    distance: 200,
    yOffset: 7,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 1,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 144,
      y: 99,
      scale: 0.41,
      squash: 0.25
    },
    backWheel: {
      x: 269,
      y: 85,
      scale: 0.315,
      squash: 0.4
    }
  },
  camry: {
    canvasWidth: 320,
    canvasHeight: 480,
    x: -10,
    y: 430,
    scale: 0.775,
    startScale: 0.2,
    distance: 100,
    yOffset: 20,
    fadeDuration: 0.2,
    driveDuration: 1.9,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75
  },
  tacoma: {
    canvasWidth: 320,
    canvasHeight: 480,
    x: -70,
    y: 504,
    scale: 0.6,
    startScale: 0.2,
    distance: 310,
    yOffset: 40,
    fadeDuration: 0.3,
    driveDuration: 2,
    brakeDuration: 0.8,
    recoilDuration: 0.3,
    brakeStrength: 1,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 132,
      y: 94,
      scale: 0.3,
      squash: 0.23
    },
    backWheel: {
      x: 224,
      y: 76,
      scale: 0.28,
      squash: 0.6
    }
  }
}
