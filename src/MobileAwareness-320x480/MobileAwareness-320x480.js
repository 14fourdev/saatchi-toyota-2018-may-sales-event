import { h, Component, render } from 'preact'
import { MobileAwareness as BannerClass } from '../banner-config'
import VariationAnimations from './VariationAnimations'
import Car from '../Car/CanvasCar'
import LegalOverlay from '../LegalOverlay/LegalOverlay'
import './MobileAwareness-320x480.scss'

class MobileAwareness320x480 extends BannerClass {
  componentWillMount() {
    this.cars = {}
    this.delayShowLegal = this.delayShowLegal.bind(this)
    this.interruptShowLegal = this.interruptShowLegal.bind(this)
    this.showLegal = this.showLegal.bind(this)
  }

  animation(timeline) {
    timeline
      .to(this.refs.banner, 0.3, { autoAlpha: 1 })
      .to(this.refs.wipeContainer1, 0.8, { height: '100%', ease: Power1.easeIn })
      .to(this.refs.wipeContainer2, 0.8, { height: '100%', ease: Power0.easeNone }, 0.5)
      .from(this.refs.eventDate, 0.7, { y: -200, ease: Power1.easeOut }, 1.25)
      .from(this.refs.eventCopy, 0.7, { y: -300, ease: Power1.easeOut }, 1.3)
      .add(this.cars.middle.getTimeline(), 1.5)
      .add(this.cars.left.getTimeline(), 2.7)
      .add(this.cars.right.getTimeline(), 3)
  }

  animationComplete() {
    // animation finished
  }

  delayShowLegal() {
    this.timer = setTimeout(this.showLegal, 750)
  }

  interruptShowLegal() {
    clearTimeout(this.timer)
  }

  showLegal() {
    this.overlayRef.show()
  }

  render() {
    return (
      <div className="banner MobileAwareness-320x480 hidden" data-ref="banner">
        <div className="border border-n" />
        <div className="border border-e" />
        <div className="border border-s" />
        <div className="border border-w" />
        <div className="wipe-container" data-ref="wipeContainer1">
          <img className="abs background" src={this.image('mobileawareness_320x480_background.png')} />
          <div className="wipe-container" data-ref="wipeContainer2">
            <img className="abs background-accent" src={this.image('mobileawareness_320x480_background-accent.png')} />
          </div>
        <Car
          ref={(r) => { this.cars.right = r }}
          {...VariationAnimations['tacoma']}
          body={this.image(`mobileawareness_320x480_tacoma-body.png`)}
          wheel={this.image(`mobileawareness_320x480_tacoma-drive.png`)}
          rim={this.image(`mobileawareness_*_tacoma-wheel.png`)}
          lights={this.image(`mobileawareness_320x480_tacoma-drl.png`)}
        />
        <Car
          ref={(r) => { this.cars.left = r }}
          {...VariationAnimations['rav4']}
          body={this.image(`mobileawareness_*_rav4-body.png`)}
          wheel={this.image(`mobileawareness_*_rav4-drive.png`)}
          rim={this.image(`mobileawareness_*_rav4-wheel.png`)}
        />
        <Car
          ref={(r) => { this.cars.middle = r }}
          {...VariationAnimations['camry']}
          body={this.image(`mobileawareness_320x480_camry-body.png`)}
          wheel={this.image(`mobileawareness_320x480_camry-drive.png`)}
          lights={this.image(`mobileawareness_320x480_camry-drl.png`)}
        />
          <img className="abs event-copy" data-ref="eventCopy" src={this.image('mobileawareness_320x480_event-copy.png')} />
          <div className="event-date" data-ref="eventDate">Offers end June 4</div>
          <img className="abs toyota-logo" src={this.image('mobileawareness_320x480_toyota-logo.png')} />
          <div className="cta-container">
            <div className="cta" data-ref="cta">Learn More</div>
          </div>
          <div className="legal">
            <div className="disclosure">Options shown.<br/>Participating dealers only.</div>
          </div>
        </div>
        <LegalOverlay
          ref={(r) => { this.overlayRef = r }}
        />
      </div>
    )
  }
}

export default MobileAwareness320x480
