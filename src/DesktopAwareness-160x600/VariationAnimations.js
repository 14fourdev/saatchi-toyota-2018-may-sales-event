export default {
  camry: {
    canvasWidth: 160,
    canvasHeight: 600,
    x: -64,
    y: 632,
    scale: 0.97,
    startScale: 0.3,
    distance: 100,
    yOffset: -30,
    fadeDuration: 0.2,
    driveDuration: 1.7,
    brakeDuration: 0.8,
    recoilDuration: 0.4,
    brakeStrength: 0.75,
    rimSize: 100,
    rotateSpeed: 1,
    frontWheel: {
      x: 286,
      y: 117,
      scale: 0.62,
      squash: 0.75
    },
    backWheel: {
      x: 360.5,
      y: 106,
      scale: 0.41,
      squash: 0.8
    }
  }
}
